import './App.css';
import { Route, Routes, useLocation, useNavigate } from 'react-router-dom';
import Home from './slide/Home';
import Buy from './slide/Buy';
import SearchItem from './slide/SearchItem';
import Login from './component/Signin';
import Register from './component/Signup';
import KategoriSearch from './component/KategoriSearch.js';
import UserKategoriSearch from './component/KategoriSearch2.js';
import Checkout from './component/Checkout.js';

// import Busket from './slide/Busket';
import Verification from './slide/Verification';
import { NoUserRoote, AdminRoote, UserRoote, NotFount } from './roote';
import { UserBusket, UserBuy, UserHome, UserSearchItem } from './slide';
import { useEffect, useState } from 'react';
import { APIRequest, setAuthToken, getAuthToken } from './axios';
import { InvoiceTable } from './component/InvoiceTable';
import { DetailInvoiceTable } from './component/DetailInvoiceTable';
import './App.css';

function App() {
  const location = useLocation();
  const page = useNavigate();

  const [login, setLogin] = useState(false);

  const testToken = (changePage) => {
    // const isLogin = localStorage.getItem('@login');
    const jwtToken = getAuthToken();
    setAuthToken(jwtToken);

    APIRequest({
      method: 'get',
      url: 'api/users/testtoken',
    })
      .then((res) => {
        if (res.status == 200) {
          localStorage.setItem('@login', true);
          setLogin(true);
          if (!changePage && login) {
            if (localStorage.getItem('@idRole') == '1') {
              page('/admin');
            } else {
              page('/user');
            }
          }
        }
      })
      .catch((err) => {
        if (login && localStorage.getItem('@login') == 'true') {
          alert('Sesi Anda Telah Berakhir, silahkan lakukan login ulang');
          page('/login');

          localStorage.setItem('@login', false);

          // setLogin(localStorage.getItem('@login'));
          setLogin(false);
          setAuthToken('');
        }
      });
  };

  useEffect(() => {
    if (
      location.pathname !== '/login' &&
      location.pathname !== '/register' &&
      location.pathname !== '/verification'
    ) {
      // setChangePage(true);
      testToken(true);
    }
    window.scrollTo(0, 0);
  }, [location]);

  // useEffect(() => {
  //   testToken();
  // }, []);

  return (
    <div className="App">
      <Routes>
        {/* No User */}
        <Route path="/" element={<NoUserRoote />}>
          <Route index element={<Home />} />
          <Route path="buy/:id" element={<Buy />} />
          <Route path="search" element={<SearchItem />} />
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route path="checkout" element={<Checkout />} />
          {/* <Route path="invoice" element={<InvoiceTable />} /> */}
          {/* <Route path="detail" element={<DetailInvoiceTable />} /> */}
          <Route path="kategorisearch" element={<KategoriSearch />} />
        </Route>

        {/* User */}
        {login && location.pathname.includes('/user') ? (
          <Route path="/user" element={<UserRoote />}>
            <Route index element={<UserHome />} />
            <Route path="buy/:id" element={<UserBuy />} />
            <Route path="busket" element={<UserBusket />} />
            <Route path="search" element={<UserSearchItem />} />
            <Route path="kategorisearch" element={<UserKategoriSearch />} />
            <Route path="payment" element={<Checkout />} />
            <Route path="invoice" element={<InvoiceTable />} />
            <Route path="detail" element={<DetailInvoiceTable />} />
          </Route>
        ) : (
          <Route path="/user" element={<div />}>
            <Route index element={<div />} />
            <Route path="buy/:id" element={<div />} />
            <Route path="busket" element={<div />} />
            <Route path="search" element={<div />} />
            <Route path="kategorisearch" element={<div />} />
            <Route path="invoice" element={<div />} />
          </Route>
        )}

        {/* Admin */}
        {login && location.pathname.includes('/admin') ? (
          <Route path="/admin" element={<AdminRoote />} />
        ) : (
          <Route path="/admin" element={<div />} />
        )}

        {/* Verification */}
        <Route path="/verification/:token" element={<Verification />} />

        {/* Not Found */}
        <Route path="*" element={<NotFount />} />
      </Routes>
    </div>
  );
}

export default App;
