import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { LoadingButton } from '@mui/lab';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

// API Request
import { APIRequest } from '../axios';

// Navigate
import { useNavigate } from 'react-router-dom';

const theme = createTheme();

export default function SignUp() {
  const MySwal = withReactContent(Swal);
  const [loading, setLoading] = React.useState(false);
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [confirmPassword, setConfirmPassword] = React.useState('');
  const [firstName, setFirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');

  // Navigate
  const page = useNavigate();

  const Register = () => {
    setLoading(true);
    APIRequest({
      method: 'post',
      url: 'api/Users/Register',
      data: {
        email: email,
        password: password,
        first_name: firstName,
        last_name: lastName,
        id_role: 2,
      },
    })
      .then((res) => {
        if (res.status == 200) {
          MySwal.fire({
            icon: 'success',
            title: 'Berhasil..',
            text: 'Registrasi akun berhasil, harap periksa email untuk proses verifikasi.',
          });
          page('/login', { state: email });
        }
      })
      .catch((err) => {
        MySwal.fire({
            icon: 'error',
            title: 'Oops..',
            text: 'Registrasi gagal, ' + err.response.data,
          });
      })
      .finally(() => setLoading(false));
  };

  const checkPassword = () => {
    if (password === confirmPassword) {
      Register();
    } else {
      MySwal.fire({
            icon: 'error',
            title: 'Oops..',
            text: 'Password salah, harap periksa kembali.'
          });
    }
  };

  return (
    <div style={{ marginTop: '120px', marginBottom: '100px' }}>
      <ThemeProvider theme={theme}>
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: '#39AB6D' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Daftar Akun
            </Typography>
            <Box noValidate sx={{ mt: 3 }}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="given-name"
                    name="firstName"
                    required
                    fullWidth
                    id="firstName"
                    label="Nama Depan"
                    autoFocus
                    color="success"
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    required
                    fullWidth
                    id="lastName"
                    label="Nama Belakang"
                    name="lastName"
                    autoComplete="family-name"
                    onChange={(e) => setLastName(e.target.value)}
                    color="success"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="email"
                    label="Email"
                    name="email"
                    onChange={(e) => setEmail(e.target.value)}
                    autoComplete="email"
                    color="success"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    onChange={(e) => setPassword(e.target.value)}
                    autoComplete="new-password"
                    color="success"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="confirmPassword"
                    label="Konfirmasi Password"
                    type="password"
                    id="confirmPassword"
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    autoComplete="confirm-password"
                    color="success"
                  />
                </Grid>
                <Grid item xs={12}>
                  <FormControlLabel
                    control={
                      <Checkbox value="allowExtraEmails" color="success" />
                    }
                    label="Saya ingin menerima inspirasi, promosi pemasaran, dan pembaruan melalui email."
                  />
                </Grid>
              </Grid>

              <LoadingButton
                loading={loading}
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2, backgroundColor: '#39AB6D' }}
                onClick={() => checkPassword()}
              >
                Daftar
              </LoadingButton>
              <Grid container justifyContent="flex-end">
                <Grid item>
                  <Link
                    onClick={() => page('/login')}
                    style={{ cursor: 'pointer' }}
                    variant="body2"
                  >
                    Sudah memiliki akun? Masuk
                  </Link>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Container>
      </ThemeProvider>
    </div>
  );
}
