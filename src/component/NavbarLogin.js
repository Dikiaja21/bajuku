import { useState, useEffect } from 'react';
import { AppBar, Button, Toolbar, TextField, IconButton } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import MenuIcon from '@mui/icons-material/Menu';
import './css/navbar.css';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import Exit from './assets/images/exit.svg';
import Invoice from './assets/images/list.svg';
import { setAuthToken } from '../axios';

const ExitIcon = () => {
  return <img src={Exit} style={{ height: 25, width: 25 }} alt="Exit" />;
};

const InvoiceIcon = () => {
  return <img src={Invoice} style={{ height: 25, width: 25 }} alt="Exit" />;
};

const NavbarLogin = () => {
  const [menu, setMenu] = useState(true);
  const [open, setOpen] = useState(false);
  const [onCategory, setOnCategory] = useState(false);
  const login = useNavigate();
  const register = useNavigate();
  const page = useNavigate();
  const cari = useNavigate();
  const [search, setSearch] = useState('');
  const location = useLocation();

  let navbarToggle = ['button-navbar'];
  let searchToggle = ['search-navbar', 'search-active'];
  let searchIcon = ['icon-responsive', 'search-ir'];
  let titleMenu = ['logo-navbar'];

  const searchText = (input) => {
    setSearch(input);
  };

  if (menu) {
    navbarToggle = ['button-navbar'];
  } else {
    navbarToggle.push('button-navbar-active');
  }

  if (open) {
    searchToggle = ['search-navbar'];
    titleMenu.push('logo-navbar-active');
    searchIcon.push('search-none');
  } else {
    searchToggle.push('search-active');
    titleMenu = ['logo-navbar'];
  }

  const handleInvoice = (item = 'false') => {
    // saya ganti sementara
    if (item === 'true') {
      page('/user/invoice');
    } else {
      alert(
        'Maaf anda belum login, silahkan lakukan login atau register terlebih dahulu'
      );
      page('/login');
    }
  };

  const exite = () => {
    // saya ganti sementara
    page('/login');
    setAuthToken('');
    localStorage.setItem('@idUser', '');
    localStorage.setItem('@idRole', '');
  };

  useEffect(() => {
    if (location.pathname !== '/search') setSearch('');
    if (location.pathname == '/user/kategorisearch') {
      setOnCategory(true);
    } else {
      setOnCategory(false);
    }
  }, [location]);

  return (
    <div>
      <AppBar>
        <AppBar style={{ backgroundColor: '#ffffff', padding: '10px' }}>
          <Toolbar>
            {/* ini diganti */}
            <Link to={'/user'}>
              <h1 className={titleMenu.join(' ')}>Bajuku</h1>
            </Link>

            <div
              style={{
                marginLeft: 'auto',
                display: 'flex',
                gap: '20px',
                alignItems: 'center',
                position: 'relative',
              }}
            >
              <div className={searchToggle.join(' ')}>
                <TextField
                  onBlur={(e) => {
                    setOpen(false);
                  }}
                  value={search}
                  color="success"
                  className="search-navbar-input"
                  label="Cari aja disini"
                  onChange={(e) => {
                    searchText(e.target.value);
                  }}
                  InputProps={{
                    endAdornment: (
                      <IconButton
                        onClick={() => {
                          if (onCategory == true) {
                            cari('/user/kategorisearch', {
                              state: {
                                search: search,
                                id_category:
                                  localStorage.getItem('@id_category'),
                              },
                            });
                          } else {
                            cari('/user/search', {
                              replace: true,
                              state: { search },
                            });
                          }
                        }}
                      >
                        <SearchIcon style={{ color: '#39ab6d' }} />
                      </IconButton>
                    ),
                  }}
                />
              </div>

              <div className={searchIcon.join(' ')}>
                <IconButton
                  onClick={() => {
                    setOpen(!open);
                  }}
                >
                  <SearchIcon style={{ color: '#39ab6d' }} />
                </IconButton>
              </div>

              <div className="navblok">
                <IconButton
                  style={{
                    width: '50px',
                    height: '50px',
                    backgroundColor: '#ffffff',
                    marginLeft: '13px',
                  }}
                  onClick={() => handleInvoice(localStorage.getItem('@login'))}
                >
                  <InvoiceIcon />
                </IconButton>
                <IconButton
                  style={{
                    width: '50px',
                    height: '50px',
                    backgroundColor: '#ffffff',
                  }}
                  onClick={() => {
                    setMenu(!menu);
                    exite();
                    localStorage.setItem('@login', false);
                  }}
                >
                  <ExitIcon />
                </IconButton>
              </div>
            </div>

            <div className="icon-responsive">
              <IconButton
                onClick={() => {
                  setMenu(!menu);
                }}
              >
                <MenuIcon style={{ color: 'black' }} />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
        <div className={navbarToggle.join(' ')}>
          <Button
            onClick={() => {
              login('/user/invoice');
              setMenu(true);
            }}
            variant="contained"
          >
            Invoice
          </Button>
          <Button
            onClick={() => {
              setMenu(!menu);
              exite();
              localStorage.setItem('@login', false);
            }}
            variant="contained"
            style={{ backgroundColor: 'red', color: 'white' }}
          >
            Keluar
          </Button>
        </div>
      </AppBar>
    </div>
  );
};

export default NavbarLogin;
