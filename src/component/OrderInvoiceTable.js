import React, { useState, useEffect } from 'react';
import Button from '@mui/material/Button';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import { styled } from '@mui/material/styles';
import TableRow from '@mui/material/TableRow';
import { APIRequest } from '../axios';
import Title from './Title';

const ActionButton = styled(Button)({
  boxShadow: 'none',
  color: 'white',
  textTransform: 'none',
  fontSize: { xs: '14px', md: '18px' },
  padding: '6px 12px',
  border: '1px solid',
  borderRadius: '8px',
  lineHeight: '21px',
  backgroundColor: '#39AB6D',
  borderColor: '#39AB6D',
  width: '180px',
  fontFamily: 'Work Sans',
  fontStyle: 'normal',
  fontWeight: 500,
  '&:hover': {
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6Dc',
    boxShadow: 'none',
  },
  '&:active': {
    boxShadow: 'none',
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6D',
  },
});

function preventDefault(event) {
  event.preventDefault();
}

export default function OrderInvoiceTable() {
  const [listData, setListData] = useState([]);

  const updateStatus = (id, status) => {
    console.log(id, status);
    APIRequest({
      method: 'post',
      url: 'api/invoice/updatestatus',
      params: {
        id: id,
        status: status,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          console.log('Berhasil');
          getInvoiceTable();
        }
      })
      .catch((err) => {
        console.log('Gagal Balikan', err.response.data);
      });
  };

  // Menghubungkan dengan API & Mengirimkan data tabel
  const getInvoiceTable = () => {
    APIRequest({
      method: 'get',
      url: 'api/Invoice/GetInvoiceAdminTest',
    })
      .then((res) => {
        if (res.status === 200) {
          setListData(res.data);
        }
      })
      .catch((err) => {
        console.log('Gagal', err.response.data);
      });
  };

  useEffect(() => {
    getInvoiceTable();
  }, []);

  return (
    <React.Fragment>
      <Title>Daftar Order Invoice</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell
              align="center"
              sx={{ fontFamily: 'Work Sans', fontWeight: 600 }}
            >
              No.
            </TableCell>
            <TableCell
              align="center"
              sx={{ fontFamily: 'Work Sans', fontWeight: 600 }}
            >
              Order Invoice Id
            </TableCell>
            <TableCell
              align="center"
              sx={{ fontFamily: 'Work Sans', fontWeight: 600 }}
            >
              {' '}
              Nama Pembeli
            </TableCell>
            <TableCell
              align="center"
              sx={{ fontFamily: 'Work Sans', fontWeight: 600 }}
            >
              Quantity
            </TableCell>
            <TableCell
              align="center"
              sx={{ fontFamily: 'Work Sans', fontWeight: 600 }}
            >
              Total Harga
            </TableCell>
            <TableCell
              align="center"
              sx={{ fontFamily: 'Work Sans', fontWeight: 600 }}
            >
              Status
            </TableCell>
            <TableCell
              align="center"
              sx={{ fontFamily: 'Work Sans', fontWeight: 600 }}
            >
              Action
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {listData.map((item, index) => (
            <TableRow key={item.id}>
              <TableCell align="center" sx={{ fontFamily: 'Work Sans' }}>
                {index + 1}
              </TableCell>
              <TableCell align="center" sx={{ fontFamily: 'Work Sans' }}>
                {item.id}
              </TableCell>
              <TableCell align="center" sx={{ fontFamily: 'Work Sans' }}>
                {item.customer_name}
              </TableCell>
              <TableCell align="center" sx={{ fontFamily: 'Work Sans' }}>
                {item.total_quantity}
              </TableCell>
              <TableCell align="center" sx={{ fontFamily: 'Work Sans' }}>
                {`Rp${item.total_price}`}
              </TableCell>
              <TableCell align="center" sx={{ fontFamily: 'Work Sans' }}>
                {item.status}
              </TableCell>
              <TableCell align="center" sx={{ fontFamily: 'Work Sans' }}>
                <ActionButton
                  onClick={() => {
                    updateStatus(item.id, 'Confirmed');
                  }}
                >
                  Konfirmasi Pesanan
                </ActionButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </React.Fragment>
  );
}
