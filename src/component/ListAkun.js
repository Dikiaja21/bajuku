import React, { useState, useEffect } from 'react';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from './Title';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import { APIRequest } from '../axios';
import MyDialog from './child/Dialog';
import { emptyDataList, emptyActionType } from '../types';
import AccountDialog from './child/AccountDialog';

const ActionButton = styled(Button)({
  boxShadow: 'none',
  color: 'white',
  textTransform: 'none',
  fontSize: { xs: '14px', md: '18px' },
  padding: '6px 12px',
  border: '1px solid',
  borderRadius: '8px',
  lineHeight: '21px',
  backgroundColor: '#39AB6D',
  borderColor: '#39AB6D',
  width: '180px',
  fontFamily: 'Work Sans',
  fontStyle: 'normal',
  fontWeight: 700,
  '&:hover': {
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6Dc',
    boxShadow: 'none',
  },
  '&:active': {
    boxShadow: 'none',
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6D',
  },
});

function preventDefault(event) {
  event.preventDefault();
}

export const Orders = (
  props = {
    open: false,
    onClose: () => {},
  }
) => {
  const [listData, setListData] = useState([]);
  //useState untuk add produk
  const [open, setOpen] = useState(false);
  const [type, setType] = useState(emptyActionType);
  const [id, setId] = useState(null);
  const [coba, setCoba] = useState(10);

  const postEdite = (typee = 'edit', dataa = {}, id) => {
    APIRequest({
      method: 'post',
      url: 'api/Products/Edite',
      data: {
        title: dataa.title,
        price: dataa.price,
        description: dataa.desc,
        keywords: dataa.keywords,
        fk_category_id: dataa.category,
        image_content: dataa.imageContent,
        stok: dataa.stok,
      },
      params: {
        id: id,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          alert('Data Berhasil Diubah');
          // getData(coba);
        }
      })
      .catch((err) => {
        console.log('err post', err.response.data);
      });
  };

  const getDelete = (typee = 'delete', id) => {
    APIRequest({
      method: 'delete',
      url: 'api/Products/Delete',
      params: {
        id: id,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          alert('Data Berhasil dihapus');
          // getData(coba);
        }
      })
      .catch((err) => {
        console.log('err post', err.response.data);
      });
  };

  const postDataUser = (typee = 'add', dataa = {}) => {
    APIRequest({
      method: 'post',
      url: 'api/Users/RegisterByAdmin',
      data: {
        first_name: dataa.first_name,
        last_name: dataa.last_name,
        email: dataa.email,
        password: dataa.password,
        id_role: dataa.id_role,
        active: dataa.active,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          alert('Data Berhasil Ditambah');
          getUsersAll();
        }
      })
      .catch((err) => {
        console.log('err post', err.response.data);
      });
  };

  useEffect(() => {
    getUsersAll();
  }, []);

  // Menghubungkan dengan API & Mengupdate tipe akun
  const updateAccountType = (id, id_role) => {
    APIRequest({
      method: 'post',
      url: 'api/Users/updateaccounttype',
      params: {
        id: parseInt(id),
        id_role: parseInt(id_role),
      },
    })
      .then((res) => {
        if (res.status === 200) {
          console.log('Berhasil');
          getUsersAll();
        }
      })
      .catch((err) => {
        console.log('Gagal', err.response.data);
      });
  };

  // Menghubungkan dengan API & Mengirimkan data tabel
  const getUsersAll = () => {
    APIRequest({
      method: 'get',
      url: 'api/Users/getusersall',
    })
      .then((res) => {
        if (res.status === 200) {
          setListData(res.data);
        }
      })
      .catch((err) => {
        console.log('Gagal', err.response.data);
      });
  };

  useEffect(() => {
    getUsersAll();
  }, []);

  return (
    // props.open && (
    <React.Fragment>
      {/* Tombol Tambah Pengguna */}
      <ActionButton
        onClick={(e) => {
          e.preventDefault();
          setOpen(true);
          setType('add');
          setId(null);
        }}
        sx={{ mt: 1, mb: 2 }}
      >
        Tambah Pengguna
      </ActionButton>
      <Title>Daftar Pengguna</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell sx={{ fontFamily: 'Work Sans', fontWeight: 600 }}>
              No.
            </TableCell>
            <TableCell
              align="center"
              sx={{ fontFamily: 'Work Sans', fontWeight: 600 }}
            >
              Nama Pengguna
            </TableCell>
            <TableCell
              align="center"
              sx={{ fontFamily: 'Work Sans', fontWeight: 600 }}
            >
              {' '}
              Tipe Akun
            </TableCell>
            <TableCell
              align="center"
              sx={{ fontFamily: 'Work Sans', fontWeight: 600 }}
            >
              Aksi
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {listData.map((item, index) => (
            <TableRow key={index}>
              <TableCell sx={{ fontFamily: 'Work Sans' }}>
                {index + 1}
              </TableCell>
              <TableCell align="center" sx={{ fontFamily: 'Work Sans' }}>
                {item.first_name} {item.last_name}
              </TableCell>
              <TableCell align="center" sx={{ fontFamily: 'Work Sans' }}>
                {item.id_role == 2 ? 'Buyer' : 'Admin'}
              </TableCell>
              <TableCell align="center" sx={{ fontFamily: 'Work Sans' }}>
                <ActionButton
                  onClick={() => {
                    updateAccountType(item.id, 1);
                  }}
                >
                  Ubah Tipe Akun
                </ActionButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <AccountDialog
        initialData={id ? listData.find((val) => val.id === id) : {}}
        open={open}
        onClose={() => setOpen(false)}
        onAdd={(item) => {
          postDataUser('add', item);
        }}
        onEdit={(item) => {
          postEdite('edit', item, item.id);
        }}
        onDelete={(item) => {
          getDelete('delete', item.id);
        }}
        type={type}
      />
    </React.Fragment>
    // )
  );
};
