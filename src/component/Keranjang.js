import { Button, Grid, IconButton, Box } from '@mui/material';
import RemoveIcon from '@mui/icons-material/Remove';
import AddIcon from '@mui/icons-material/Add';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import './css/keranjang.css';
import { useState, useEffect } from 'react';
import { APIRequest, setAuthToken, getAuthToken } from '../axios';
import { emptyDataList } from '../types';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import CurrencyFormat from 'react-currency-format';
import { useNavigate } from 'react-router-dom';

const Keranjang = () => {
  const MySwal = withReactContent(Swal);
  const [listCart, setListCart] = useState(emptyDataList);
  const [firstRender, setFirstRender] = useState(false);
  const [tempValue, setTempValue] = useState([]);
  const [valueChecklist, setValueChecklist] = useState([]);
  const [isChecklist, setIsChecklist] = useState({});
  const [totalQty, setTotalQty] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const page = useNavigate();

  const getCart = () => {
    const idUser = localStorage.getItem('@idUser');
    const jwtToken = getAuthToken();
    setAuthToken(jwtToken);

    // console.log(idUser);
    APIRequest({
      method: 'get',
      url: 'api/carts/getlistingcart',
      params: {
        id: idUser,
      },
    })
      .then((res) => {
        if (res.status == 200) {
          setListCart(res.data);
        }
      })
      .catch((err) => {
        console.log('error');
      });
  };

  const deleteItemCart = (id_cart) => {
    APIRequest({
      method: 'post',
      url: 'api/carts/DeleteOneCard',
      params: {
        id: id_cart,
      },
    })
      .then((res) => {
        if (res.status == 200) {
          // console.log(id_cart);
          setListCart(listCart.filter((item) => item.id !== id_cart));
          setTempValue(tempValue.filter((item) => item.id !== id_cart));
          setValueChecklist(
            valueChecklist.filter((item) => item.id !== id_cart)
          );
        }
      })
      .catch((err) => {
        console.log('error');
      });
  };

  const deleteAllItemCart = () => {
    const idUser = localStorage.getItem('@idUser');
    const jwtToken = getAuthToken();
    setAuthToken(jwtToken);

    APIRequest({
      method: 'post',
      url: 'api/carts/deletecartbyiduser',
      params: {
        id: localStorage.getItem('@idUser'),
      },
    })
      .then((res) => {
        if (res.status == 200) {
          getCart();
        }
      })
      .catch((err) => {
        console.log('error');
      });
  };

  // Untuk handle check ALL

  const handleCheckAll = async (e) => {
    const isCheck = e.target.checked;
    if (isCheck) {
      // Otomatis merubah semua data onCheck di tempValue = true
      let updatedList = tempValue.map((item) => {
        return { ...item, onCheck: true };
      });
      await setTempValue(updatedList);

      let updateChecklistValue = tempValue.map((item) => {
        return {
          id: item.id,
          data: {
            id_product: item.id_product,
            category_name: item.category_name,
            title: item.title,
            quantity: item.quantity,
            price: item.price,
          },
        };
      });

      setValueChecklist(updateChecklistValue);

      // console.log(updateChecklistValue);
    } else {
      // Otomatis merubah semua data onCheck di tempValue = false
      let updatedList = tempValue.map((item) => {
        return { ...item, onCheck: false };
      });
      setTempValue(updatedList);

      setValueChecklist([]);
    }
  };

  // Untuk handle check per item
  const handleCheck = (e, index, key, data) => {
    const isCheck = e.target.checked;
    if (isCheck) {
      const tempData = tempValue.filter((item) => item.id == data.id);

      const temp = {};
      temp.id = key;
      temp.data = {
        id_product: data.id_product,
        category_name: data.category_name,
        title: data.title,
        quantity: tempData[0].quantity,
        price: data.price,
      };

      setValueChecklist(valueChecklist.concat(temp));
      const list = [...tempValue];
      list[index].onCheck = true;
      setTempValue(list);
    } else {
      setValueChecklist(valueChecklist.filter((item) => item.id !== key));

      const list = [...tempValue];
      list[index].onCheck = false;
      setTempValue(list);
    }
  };

  const handleBuyNow = () => {
    if (valueChecklist.length == 0) {
      MySwal.fire({
        icon: 'info',
        title: 'Info',
        text: 'Tidak ada item yang dipilih, silahkan pilih min 1 item.'
      });
    } else {
      page('/user/payment', { state: { data: valueChecklist, page: '/cart' } });
    }
  };

  // Update Qty By Index and Id Product
  const handleEditQty = (mode, index, id) => {
    const tempObj = [...tempValue];

    // Jika Tambah Quantity
    if (mode == 'add') {
      // Jika Item dalam kondisi checked maka tambah qty di state valueChecklist
      if (tempObj[index].onCheck == true) {
        let updatedList = valueChecklist.map((item) => {
          if (item.id == id) {
            return {
              ...item,
              data: { ...item.data, quantity: item.data.quantity + 1 },
            };
          }
          return item;
        });
        setValueChecklist(updatedList);
      }
      tempObj[index].quantity += 1;
      setTempValue(tempObj);
    } else {
      // Jika Kurang Quantity
      // Quantity harus diatas 1
      if (tempObj[index].quantity > 1) {
        // Jika Item dalam kondisi checked maka kurang qty di state valueChecklist
        if (tempObj[index].onCheck == true) {
          let updatedList = valueChecklist.map((item) => {
            if (item.id == id) {
              return {
                ...item,
                data: { ...item.data, quantity: item.data.quantity - 1 },
              };
            }
            return item;
          });
          setValueChecklist(updatedList);
        }
        tempObj[index].quantity -= 1;
        setTempValue(tempObj);
      }
    }
  };

  const handleClickDelete = (id_cart) => {
    Swal.fire({
      title: 'Apakah kamu yakin?',
      text: 'Item yang dipilih akan terhapus dari keranjang..',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, hapus!',
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Terhapus!',
          'Yeay. Item yang kamu pilih berhasil terhapus.',
          'success'
        );
        deleteItemCart(id_cart);
      }
    });
  };

  const handleClickAllDelete = () => {
    Swal.fire({
      title: 'Apa kamu yakin?',
      text: 'Semua item yang ada di keranjang mu akan dihapus..',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Terhapus!',
          'Yeay, semua itemu berhasil terhapus.',
          'success'
        );
        deleteAllItemCart();
        setFirstRender(false);
        setValueChecklist([]);
        window.setScroll(0, 0);
      }
    });
  };

  const calcTotalQty = () => {
    let result = 0;
    if (valueChecklist.length !== 0) {
      valueChecklist.forEach((item) => {
        result += item.data.quantity;
      });
    }

    setTotalQty(result);
  };

  const calcTotalPrice = () => {
    let result = 0;
    if (valueChecklist.length !== 0) {
      valueChecklist.forEach((item) => {
        result += item.data.price * item.data.quantity;
      });
    }
    setTotalPrice(result);
  };

  const fillTemp = () => {
    const temp = [];
    listCart.forEach((item) => {
      temp.push({
        id: item.id,
        category_name: item.category_name,
        id_product: item.id_product,
        title: item.title,
        quantity: item.quantity,
        price: item.price,
        onCheck: false,
      });
    });
    setTempValue(tempValue.concat(temp));
  };

  useEffect(() => {
    getCart();
  }, []);

  useEffect(() => {
    console.log(listCart);
  }, [listCart]);

  useEffect(() => {
    // console.log("Total QTY:", totalQty)
  }, [totalQty]);

  useEffect(() => {
    // console.log(tempValue);
  }, [tempValue]);

  useEffect(() => {
    console.log(valueChecklist);

    // Kalkulasi ulang Total Harga dan Total QTY jika state valueChecklist berubah
    calcTotalQty();
    calcTotalPrice();
  }, [valueChecklist]);

  useEffect(() => {
    if (listCart.length > 0 && firstRender == false) {
      fillTemp();
      setFirstRender(true);
    }
  }, [listCart]);

  const backToBuy = useNavigate();

  return (
    <div className="keranjang">
      <Grid container spacing={3}>
        <Grid item xs={12} className="title-keranjang">
          <h1>Keranjang Belanjaanmu</h1>
        </Grid>

        <Grid item xs={12}>
          <Grid container spacing={5}>
            <Grid item xs={12} md={9}>
              <Grid container spacing={2}>
                <Grid item xs={12} className="pilih-semua">
                  <div style={{ display: 'flex', alignItem: 'center' }}>
                    <input
                      type="checkbox"
                      id="pilihSemua"
                      onChange={handleCheckAll}
                    />
                    <label for="pilihSemua">Pilih Semua</label>
                  </div>

                  <Button onClick={handleClickAllDelete}>Hapus Semua</Button>
                </Grid>

                {firstRender ? (
                  listCart.map((item, index) => {
                    return (
                      <Grid item xs={12} key={index}>
                        <Grid container className="list-keranjang" spacing={2}>
                          <Grid item xs={3} md={0.5}>
                            <input
                              type="checkbox"
                              checked={tempValue[index].onCheck}
                              id="pilih"
                              onChange={(e) =>
                                handleCheck(e, index, item.id, item)
                              }
                            />
                          </Grid>

                          <Grid item xs={9} md={2}>
                            <label for="pilih">
                              <img src={item.image_content} alt="1.png" />
                            </label>
                          </Grid>

                          <Grid item xs={12} md={6.5}>
                            <div className="name-produk">
                              <div>
                                <p style={{ fontWeight: '500' }}>
                                  {item.title}
                                </p>
                              </div>
                              <div>
                                <CurrencyFormat
                                  value={item.price}
                                  displayType={'text'}
                                  thousandSeparator={true}
                                  prefix={'Rp. '}
                                  renderText={(value) => (
                                    <p style={{ fontWeight: '600' }}>{value}</p>
                                  )}
                                />
                              </div>
                            </div>
                          </Grid>

                          <Grid item xs={12} md={3}>
                            <div className="button-keranjang">
                              <div className="tools-keranjang">
                                <IconButton
                                  onClick={() => {
                                    handleEditQty('delete', index, item.id);
                                  }}
                                >
                                  <RemoveIcon />
                                </IconButton>

                                <h1>{tempValue[index].quantity}</h1>

                                <IconButton
                                  onClick={() =>
                                    handleEditQty('add', index, item.id)
                                  }
                                >
                                  <AddIcon style={{ color: '#4F4F4F' }} />
                                </IconButton>
                              </div>
                              <div className="remove-keranjang">
                                <p style={{ fontWeight: '500' }}>Delete</p>

                                <IconButton
                                  onClick={() => handleClickDelete(item.id)}
                                >
                                  <DeleteForeverIcon className="tong" />
                                </IconButton>
                              </div>
                            </div>
                          </Grid>
                        </Grid>
                      </Grid>
                    );
                  })
                ) : (
                  <Grid item xs={12}>
                    <Grid container rowSpacing={5} marginTop={5}>
                      <Grid item xs={12}>
                        <p
                          style={{
                            fontSize: '28px',
                            color: '#4F4F4F',
                            fontWeight: '600',
                            textAlign: 'center',
                          }}
                        >
                          Tidak ada barang yang terjual
                        </p>
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        style={{ display: 'flex', justifyContent: 'center' }}
                      >
                        <Button
                          style={{
                            backgroundColor: '#39AB6D',
                            color: 'white',
                            fontSize: '20px',
                            fontWeight: '700',
                            textTransform: 'capitalize',
                            width: '40%',
                          }}
                          onClick={() => {
                            backToBuy('/user');
                          }}
                        >
                          Berburu Barang yuk
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                )}
              </Grid>
            </Grid>

            <Grid item xs={12} md={3}>
              <Box className="box-keranjang">
                {firstRender ? (
                  <Grid container spacing={4}>
                    <Grid item xs={12}>
                      <p style={{ fontWeight: '700' }}>Rincian Belanjaanmu</p>
                    </Grid>

                    <Grid item xs={12}>
                      <div className="total-barang">
                        <p style={{ fontWeight: '400', color: '#828282' }}>
                          Total (Barang)
                        </p>
                        <p style={{ fontWeight: '400', color: '#828282' }}>
                          {totalQty}
                        </p>
                      </div>
                    </Grid>

                    <Grid item xs={12}>
                      <div className="total-harga">
                        <p style={{ fontWeight: '700' }}>Total (Harga)</p>
                        <CurrencyFormat
                          value={totalPrice}
                          displayType={'text'}
                          thousandSeparator={true}
                          prefix={'Rp. '}
                          renderText={(value) => (
                            <p style={{ fontWeight: '700' }}>{value}</p>
                          )}
                        />
                      </div>
                    </Grid>

                    <Grid item xs={12}>
                      <Button onClick={handleBuyNow}>Beli Sekarang</Button>
                    </Grid>
                  </Grid>
                ) : (
                  <Grid container spacing={4}>
                    <Grid item xs={12}>
                      <p style={{ fontWeight: '700' }}>Rincian Belanjaanmu</p>
                    </Grid>

                    <Grid item xs={12}>
                      <div className="total-barang">
                        <p style={{ fontWeight: '400', color: '#828282' }}>
                          Total (Barang)
                        </p>
                        <p style={{ fontWeight: '400', color: '#828282' }}>
                          {totalQty}
                        </p>
                      </div>
                    </Grid>

                    <Grid item xs={12}>
                      <div className="total-harga">
                        <p style={{ fontWeight: '700' }}>Total (Harga)</p>
                        <CurrencyFormat
                          value={totalPrice}
                          displayType={'text'}
                          thousandSeparator={true}
                          prefix={'Rp. '}
                          renderText={(value) => (
                            <p style={{ fontWeight: '700' }}>{value}</p>
                          )}
                        />
                      </div>
                    </Grid>

                    <Grid item xs={12}>
                      <Button
                        style={{ backgroundColor: '#BDBDBD', color: '#fff' }}
                      >
                        Beli Sekarang
                      </Button>
                    </Grid>
                  </Grid>
                )}
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default Keranjang;
