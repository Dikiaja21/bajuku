import React, { Component } from 'react';
import Slider from 'react-slick';
import './css/client.css';

export default class Client extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      autoplay: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    return (
      <div className="bg-container" style={{ backgroundColor: '#F1FFF8' }}>
        <Slider {...settings}>
          <div>
            <img
              className="imagesClient"
              src={require('./images/promotion-5.png')}
              alt="images1"
            />
          </div>
          <div>
            <img
              className="imagesClient"
              src={require('./images/promotion-6.png')}
              alt="images1"
            />
          </div>
          <div>
            <img
              className="imagesClient"
              src={require('./images/promotion-7.png')}
              alt="images1"
            />
          </div>
          <div>
            <img
              className="imagesClient"
              src={require('./images/promotion-8.png')}
              alt="images1"
            />
          </div>
          <div>
            <img
              className="imagesClient"
              src={require('./images/promotion-9.png')}
              alt="images1"
            />
          </div>
        </Slider>
      </div>
    );
  }
}
