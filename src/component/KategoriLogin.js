import { useState } from 'react';
import { Grid } from '@mui/material';
import { Box } from '@mui/system';
import { CategorySliderData } from './CategorySliderData';
import { FaAngleLeft, FaAngleRight } from 'react-icons/fa';
import './css/kategori.css';
import './css/categoryslider2.css';
import { useNavigate } from 'react-router-dom';

const Kategori = () => {
  const gambar = [
    {
      id: 2,
      gambar: require('./images/hat.jpg'),
      title: 'Head',
    },
    {
      id: 1,
      gambar: require('./images/shirt.png'),
      title: 'Body',
    },
    {
      id: 3,
      gambar: require('./images/pants.png'),
      title: 'Pants',
    },
    {
      id: 4,
      gambar: require('./images/shoes.png'),
      title: 'Shoes',
    },
  ];

  // Logika untuk Responsive
  const [current, setCurrent] = useState(0);
  const length = gambar.length;
  const page = useNavigate();

  const handleCategorySearch = (id, title) => {
    // const data = {
    //   id: id,
    //   title: title,
    // };
    // console.log(data);
    // page('/user/kategorisearch', { state: data });
    localStorage.setItem('@name_category', title);
    localStorage.setItem('@id_category', id);
    page('/user/kategorisearch', { state: { id_category: id, title: title } });

  };

  const nextSlide = () => {
    setCurrent(current === length - 1 ? 0 : current + 1);
  };

  const prevSlide = () => {
    setCurrent(current === 0 ? length - 1 : current + 1);
  };

  if (!Array.isArray(gambar) || gambar.length <= 0) {
    return null;
  }

  return (
    <div className="container-kategori">
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <h1>Kategori</h1>
        </Grid>
        <Grid item xs={12}>
          <Box className="respon-category">
            <Grid container rowSpacing={3} columnSpacing={7}>
              {/* Lopping */}
              {gambar.map((item, index) => {
                return (
                  <Grid item xs={6} key={index}>
                    <div
                      className="container-btn"
                      onClick={() => handleCategorySearch(item.id, item.title)}
                      style={{ cursor: 'pointer' }}
                    >
                      <div className="container-image">
                        <img src={item.gambar} alt="1.png" />
                      </div>
                      <div className="btn-title">{item.title}</div>
                    </div>
                  </Grid>
                );
              })}
            </Grid>
          </Box>
        </Grid>
        {/* ini ya */}
        <Grid item xs={12} className="slider">
          <div className="bg">
            <Box className="category-phone">
              <FaAngleLeft className="left-arrow" onClick={prevSlide} />
              <FaAngleRight className="right-arrow" onClick={nextSlide} />
              {gambar.map((slide, index) => {
                return (
                  <div
                    className={index === current ? 'slide active' : 'slide'}
                    key={index}
                    onClick={() => handleCategorySearch(slide.id, slide.title)}
                    style={{ cursor: 'pointer' }}
                  >
                    {index === current && (
                      <a href={slide.url}>
                        <img
                          style={{ margin: 'auto' }}
                          src={slide.gambar}
                          alt="travel.png"
                          className="image"
                        />
                        <div className="category-text">
                          <p>{slide.title}</p>
                        </div>
                      </a>
                    )}
                  </div>
                );
              })}
            </Box>
          </div>
        </Grid>
        {/* sampe sini */}
      </Grid>
    </div>
  );
};

export default Kategori;
