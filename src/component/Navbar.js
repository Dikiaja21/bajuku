import React, { useState, useEffect } from 'react';
import { AppBar, Button, Toolbar, TextField, IconButton } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import MenuIcon from '@mui/icons-material/Menu';
import { styled } from '@mui/material/styles';
import './css/navbar.css';
import { Link, useNavigate, useLocation } from 'react-router-dom';

const Navbar = () => {
  const [menu, setMenu] = useState(true);
  const [open, setOpen] = useState(false);
  const login = useNavigate();
  const register = useNavigate();
  // const page = useNavigate();
  const cari = useNavigate();
  const [search, setSearch] = useState('');
  const [onCategory, setOnCategory] = useState(false);
  const location = useLocation();

  const CustomButton = styled(Button)({
    boxShadow: 'none',
    color: '#39AB6D',
    textTransform: 'none',
    fontSize: 18,
    fontStyle: 'normal',
    fontWeight: 700,
    padding: '10px',
    border: '1px solid',
    borderRadius: '8px',
    lineHeight: '21px',
    backgroundColor: 'white',
    borderColor: '#39AB6D',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: ['Work Sans'].join(','),
    '&:hover': {
      backgroundColor: '#39AB6D',
      borderColor: '#39AB6D',
      boxShadow: 'none',
      color: 'white',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#39AB6D',
      borderColor: '#39AB6D',
    },
  });

  let navbarToggle = ['button-navbar'];
  let searchToggle = ['search-navbar', 'search-active'];
  let searchIcon = ['icon-responsive', 'search-ir'];
  let titleMenu = ['logo-navbar'];

  const searchText = (input) => {
    setSearch(input);
  };

  if (menu) {
    navbarToggle = ['button-navbar'];
  } else {
    navbarToggle.push('button-navbar-active');
  }

  if (open) {
    searchToggle = ['search-navbar'];
    titleMenu.push('logo-navbar-active');
    searchIcon.push('search-none');
  } else {
    searchToggle.push('search-active');
    titleMenu = ['logo-navbar'];
  }

  useEffect(() => {
    if (location.pathname !== '/search') setSearch('');
    if (location.pathname == '/kategorisearch') {
      setOnCategory(true);
    } else {
      setOnCategory(false);
    }
  }, [location]);

  return (
    <div>
      <AppBar>
        <AppBar style={{ backgroundColor: '#ffffff', padding: '10px' }}>
          <Toolbar>
            <Link to={'/'}>
              <h1 className={titleMenu.join(' ')}>Bajuku</h1>
            </Link>

            <div
              style={{
                marginLeft: 'auto',
                display: 'flex',
                gap: '20px',
                alignItems: 'center',
                position: 'relative',
              }}
            >
              <div className={searchToggle.join(' ')}>
                <TextField
                  onBlur={(e) => {
                    setOpen(false);
                  }}
                  value={search}
                  className="search-navbar-input"
                  color="success"
                  label="Cari aja disini"
                  onChange={(e) => {
                    searchText(e.target.value);
                  }}
                  InputProps={{
                    endAdornment: (
                      <IconButton
                        onClick={() => {
                          if (onCategory == true) {
                            cari('/kategorisearch', {
                              state: {
                                search: search,
                                id_category:
                                  localStorage.getItem('@id_category'),
                              },
                            });
                          } else {
                            cari('/search', {
                              replace: true,
                              state: { search },
                            });
                          }
                        }}
                      >
                        <SearchIcon style={{ color: '#39ab6d' }} />
                      </IconButton>
                    ),
                  }}
                />
              </div>

              <div className={searchIcon.join(' ')}>
                <IconButton
                  onClick={() => {
                    setOpen(!open);
                  }}
                >
                  <SearchIcon style={{ color: '#39ab6d' }} />
                </IconButton>
              </div>

              <div className="navblok">
                <CustomButton
                  onClick={() => {
                    login('/login');
                  }}
                  variant="contained"
                >
                  Masuk
                </CustomButton>
                <CustomButton
                  onClick={() => {
                    register('/register');
                  }}
                  variant="contained"
                >
                  Daftar
                </CustomButton>
              </div>
            </div>

            <div className="icon-responsive">
              <IconButton
                onClick={() => {
                  setMenu(!menu);
                }}
              >
                <MenuIcon style={{ color: 'black' }} />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
        <div className={navbarToggle.join(' ')}>
          <Button
            onClick={() => {
              login('/login');
              setMenu(true);
            }}
            variant="contained"
          >
            Masuk
          </Button>
          <Button
            onClick={() => {
              register('/register');
              setMenu(true);
            }}
            variant="contained"
          >
            Daftar
          </Button>
        </div>
      </AppBar>
    </div>
  );
};

export default Navbar;
