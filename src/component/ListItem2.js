import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import DashboardIcon from '@mui/icons-material/Dashboard';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import DescriptionIcon from '@mui/icons-material/Description';

export const ListItem2 = (
  <React.Fragment>
    <ListItemButton to="coba1">
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <ListItemText
        primary="Dashboard"
        primaryTypographyProps={{ fontFamily: 'Work Sans' }}
      />
    </ListItemButton>
    <ListItemButton to="coba2">
      <ListItemIcon>
        <ShoppingCartIcon />
      </ListItemIcon>
      <ListItemText
        primary="Pesanan"
        primaryTypographyProps={{ fontFamily: 'Work Sans' }}
      />
    </ListItemButton>
    <ListItemButton to="coba3">
      <ListItemIcon>
        <DescriptionIcon />
      </ListItemIcon>
      <ListItemText
        primary="Produk"
        primaryTypographyProps={{ fontFamily: 'Work Sans' }}
      />
    </ListItemButton>
    <ListItemButton to="/pengiriman">
      <ListItemIcon>
        <LocalShippingIcon />
      </ListItemIcon>
      <ListItemText
        primary="Pengiriman"
        primaryTypographyProps={{ fontFamily: 'Work Sans' }}
      />
    </ListItemButton>
  </React.Fragment>
);
