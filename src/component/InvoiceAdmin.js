import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import OrderInvoiceTable from './OrderInvoiceTable';

export const Coba4 = (
  props = {
    open: false,
    onclose: () => {},
  }
) => {
  return (
    props.open && (
      <Box
        component="main"
        sx={{
          backgroundColor: (theme) =>
            theme.palette.mode === 'light'
              ? theme.palette.grey[100]
              : theme.palette.grey[900],
          flexGrow: 1,
          height: '100vh',
          overflow: 'auto',
        }}
      >
        <Toolbar />
        <Container maxWidth="x" sx={{ mt: 5, mb: 4 }}>
          <Grid container spacing={3}>
            {/* Greetings */}
            <Typography
              variant="h4"
              sx={{ fontFamily: 'Work Sans', ml: 3, mt: 3 }}
            >
              Halaman Order Invoice
            </Typography>

            {/* Order Invoice Table*/}
            <Grid item xs={12}>
              <Paper
                sx={{
                  p: 2,
                  display: 'flex',
                  flexDirection: 'column',
                  minWidth: 900,
                  overflowX: 'scroll',
                }}
              >
                <OrderInvoiceTable />
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </Box>
    )
  );
};
