import React, { useState, useEffect } from 'react';
import { Button, Grid, IconButton } from '@mui/material';
import { Box } from '@mui/system';
import RemoveIcon from '@mui/icons-material/Remove';
import AddIcon from '@mui/icons-material/Add';
import './css/buyitem.css';
import { styled } from '@mui/material/styles';
import CurrencyFormat from 'react-currency-format';
import { useLocation, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { APIRequest } from '../axios';

// Custom Button
const CustomButton = styled(Button)({
  boxShadow: 'none',
  color: '#39AB6D',
  textTransform: 'none',
  fontSize: 20,
  fontStyle: 'normal',
  fontWeight: 700,
  padding: '10px',
  border: '1px solid',
  borderRadius: '8px',
  lineHeight: '21px',
  backgroundColor: 'white',
  borderColor: '#39AB6D',
  justifyContent: 'center',
  alignItems: 'center',
  fontFamily: ['Work Sans'].join(','),
  '&:hover': {
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6D',
    boxShadow: 'none',
    color: 'white',
  },
  '&:active': {
    boxShadow: 'none',
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6D',
  },
});

const BuyItem = (
  props = {
    id: 0,
    title: '',
    category_name: '',
    desc: '',
    price: 0,
    imageContent: '',
  }
) => {
  const MySwal = withReactContent(Swal);
  const [buy, setBuy] = useState(true);
  const buttonBuy = ['detail'];
  const titleBuy = ['box-total'];
  const [price, setPrice] = useState(props.price);
  const [tempPrice, setTempPrice] = useState(props.price);
  const [qty, setQty] = useState(1);
  const navigate = useNavigate();

  const addCartItem = () => {
    // ApiRequest hasil import dari .../axios/index.js
    APIRequest({
      method: 'post',
      url: '/api/carts/addcart',
      data: {
        id_user: localStorage.getItem('@idUser'),
        id_product: props.id,
        quantity: qty,
      },
    })
      .then((res) => {
        if (res.status == 200) {
          MySwal.fire({
            icon: 'success',
            title: 'Berhasil..',
            text: 'Item berhasil di tambahkan ke keranjang',
          });
        }
      })
      .catch((err) => {
        MySwal.fire({
          icon: 'error',
          title: 'Oops..',
          text: 'Terjadi kesahalan',
        });
      });
  };

  const location = useLocation();

  useEffect(() => {
    setQty(1);
    setPrice(props.price);
    setTempPrice(props.price);
  }, [location]);

  const handleAddItem = () => {
    setQty(qty + 1);
    setPrice(price + tempPrice);
  };

  const handleRemoveItem = () => {
    if (qty > 1) {
      setQty(qty - 1);
      setPrice(price - tempPrice);
    }
  };

  const handleCart = (item = 'false') => {
    if (item == 'true') {
      addCartItem();
    } else {
      MySwal.fire({
        icon: 'info',
        title: 'Info',
        text: 'Tidak dapat akses keranjang, silahkan untuk melakukan proses login atau register akun terlebih dahulu.'
      });
      navigate('/login');
    }
  };

  const handleBuyNow = (item = 'false') => {
    const itemBuy = [
      {
        id: props.id,

        data: {
          id_product: props.id,
          category_name: props.category_name,
          title: props.title,
          quantity: qty,
          price: price,
        },
      },
    ];

    if (item == 'true') {
      navigate('/user/payment', { state: { data: itemBuy, page: '/buy' } });
    } else {
      MySwal.fire({
        icon: 'info',
        title: 'Info',
        text: 'Tidak dapat akses keranjang, silahkan untuk melakukan proses login atau register akun terlebih dahulu.'
      });
      navigate('/login');
    }
  };

  if (buy) {
    buttonBuy.push('detail-active');
  } else {
    titleBuy.push('box-total-active');
  }

  return (
    <div className="buy-item">
      <Grid container rowSpacing={2} columnSpacing={7}>
        {/* Image */}
        <Grid item xs={12} md={12} lg={4}>
          <img
            onClick={() => {
              setBuy(!buy);
            }}
            className="image-buy"
            src={props.imageContent}
            alt="1.png"
          />
        </Grid>

        {/* Detail */}
        <Grid item xs={12} md={6} lg={5}>
          <Box className={buttonBuy}>
            <Grid container direction="column" spacing={2}>
              <Grid item xs={12}>
                <h1 className="detail-title">{props.title}</h1>
              </Grid>

              <Grid item xs={12}>
                <CurrencyFormat
                  value={props.price}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp. '}
                  renderText={(value) => (
                    <h2 className="detail-price">{value}</h2>
                  )}
                />
              </Grid>

              <Grid item xs={12}>
                <p className="descript-detail" style={{ fontWeight: '700' }}>
                  Detail
                </p>
              </Grid>

              <Grid item xs={12}>
                <p className="descript-detail" style={{ fontWeight: '400' }}>
                  Kategori :{' '}
                  <span style={{ fontWeight: '700' }}>
                    {props.category_name ?? localStorage.getItem('@name_category')}
                  </span>
                </p>
              </Grid>

              <Grid item xs={12}>
                <p
                  className="descript-detail"
                  style={{ fontWeight: '400', textAlign: 'justify' }}
                >
                  {' '}
                  {props.desc}
                </p>
              </Grid>
            </Grid>
          </Box>
        </Grid>

        {/* Box Total */}
        <Grid item xs={12} md={6} lg={3}>
          <Box className={titleBuy.join(' ')}>
            <Grid container spacing={2}>
              <Grid item xs={6} md={12}>
                <h1 className="title-total">Pilih Jumlah</h1>
              </Grid>

              <Grid item xs={6} md={6}>
                <div className="tools-total">
                  <IconButton onClick={handleRemoveItem}>
                    <RemoveIcon />
                  </IconButton>

                  <h1>{qty}</h1>
                  <IconButton onClick={handleAddItem}>
                    <AddIcon />
                  </IconButton>
                </div>
              </Grid>

              <Grid item xs={12}>
                <div className="harga-total">
                  <h1 style={{ fontWeight: 400, color: '#828282 ' }}>Total</h1>
                  <CurrencyFormat
                    value={price}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'Rp. '}
                    renderText={(value) => (
                      <h1 style={{ fontWeight: 700 }}>{value}</h1>
                    )}
                  />
                </div>
              </Grid>

              <Grid item xs={12}>
                <div className="button-total">
                  <CustomButton
                    onClick={() => handleCart(localStorage.getItem('@login'))}
                  >
                    Masuk Keranjang
                  </CustomButton>
                  <CustomButton
                    onClick={() => handleBuyNow(localStorage.getItem('@login'))}
                  >
                    Beli Sekarang
                  </CustomButton>
                </div>
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};

export default BuyItem;
