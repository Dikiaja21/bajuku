import React, { useState, useEffect } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
// import AppBar from "@mui/material/AppBar";
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
// import Toolbar from "@mui/material/Toolbar";
import Paper from '@mui/material/Paper';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import AddressForm from './AddressForm';
import PaymentForm from './PaymentForm';
import Review from './Review';
import { APIRequest, setAuthToken, getAuthToken } from '../axios';
import { useLocation, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { LoadingButton } from '@mui/lab';

const steps = ['Alamat pengiriman', 'Detail pembayaran', 'Konfirmasi pesanan'];

const BootstrapButton = styled(Button)({
  boxShadow: 'none',
  textTransform: 'none',
  fontSize: 16,
  padding: '6px 12px',
  border: '1px solid',
  borderRadius: 8,
  lineHeight: 1.5,
  backgroundColor: '#39AB6D',
  borderColor: '#39AB6D',
  fontFamily: ['Work Sans'].join(','),
  '&:hover': {
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6D',
    boxShadow: 'none',
  },
  '&:active': {
    boxShadow: 'none',
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6D',
  },
});

const deleteCart = (id_user, id_product) => {
  const isLogin = localStorage.getItem('@login');
  const jwtToken = getAuthToken();
  setAuthToken(jwtToken);

  APIRequest({
    method: 'post',
    url: 'api/carts/deletecart',
    data: {
      id_user: id_user,
      id_product: id_product,
    },
  })
    .then((res) => {
      console.log('berhasil');
    })
    .catch((err) => {
      console.log('error');
    });
};

const theme = createTheme();

export default function Checkout() {
  const MySwal = withReactContent(Swal);

  const [activeStep, setActiveStep] = React.useState(0);
  const [namaDepan, setNamaDepan] = useState('');
  const [namaBelakang, setNamaBelakang] = useState('');
  const [alamat1, setAlamat1] = useState('');
  const [alamat2, setAlamat2] = useState('');
  const [kota, setKota] = useState('');
  const [provinsi, setProvinsi] = useState('');
  const [kodePos, setKodePos] = useState('');
  const [negara, setNegara] = useState('');
  const [nama, setNama] = useState('');
  const [nomorRekening, setNomorRekening] = useState('');
  const [nomorTelp, setNomorTelp] = useState('');
  const [ongkosKirim, setOngkosKirim] = useState('Free');
  const location = useLocation();
  const page = useNavigate();
  const [loading, setLoading] = useState(false);

  const handleNext = () => {
    if (activeStep === steps.length - 1) {
      // page('/user');
      if (location.state.page == '/buy') {
        handleaddInvoice();
        MySwal.fire({
          icon: 'success',
          title: 'Berhasil..',
          text: 'Transaksi berhasil dilakukan.',
        });
        page('/user');
        // console.log('ini dari buy');
      } else {
        const listProduct = location.state.data.map((item) => {
          return item.data.id_product;
        });

        deleteCart(localStorage.getItem('@idUser'), listProduct);

        let totalPrice = 0;
        let totalQty = 0;

        // Hitung Total Qty dan Total Price
        location.state.data.forEach((item) => {
          totalQty += item.data.quantity;
          totalPrice += item.data.price * item.data.quantity;
        });

        // Tampung Array khusus quantity
        const listQuantity = location.state.data.map((item) => {
          return item.data.quantity;
        });

        handleaddInvoiceCart(totalPrice, totalQty, listProduct, listQuantity);
      }
    } else {
      setActiveStep(activeStep + 1);
      window.scrollTo({
        top: 0,
        left: 0,
        behavior: 'smooth',
      });
    }
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  const handleChangeNamaDepan = (e) => {
    setNamaDepan(e.currentTarget.value);
  };

  const handleChangeNamaBelakang = (e) => {
    setNamaBelakang(e.currentTarget.value);
  };

  const handleChangeAlamat1 = (e) => {
    setAlamat1(e.currentTarget.value);
  };

  const handleChangeAlamat2 = (e) => {
    setAlamat2(e.currentTarget.value);
  };

  const handleChangeKota = (e) => {
    setKota(e.currentTarget.value);
  };

  const handleChangeProvinsi = (e) => {
    setProvinsi(e.currentTarget.value);
  };

  const handleChangeKodePos = (e) => {
    setKodePos(e.currentTarget.value);
  };

  const handleChangeNegara = (e) => {
    setNegara(e.currentTarget.value);
  };

  const handleChangeNama = (e) => {
    setNama(e.currentTarget.value);
  };

  const handleChangeNomorRekening = (e) => {
    setNomorRekening(e.currentTarget.value);
  };

  const handleChangeNomorTelp = (e) => {
    setNomorTelp(e.currentTarget.value);
  };

  const handleaddInvoice = () => {
    APIRequest({
      method: 'post',
      url: 'api/Invoice/AddInvoice',
      data: {
        id: 0,
        id_user: localStorage.getItem('@idUser'),
        nama: namaDepan + namaBelakang,
        alamat: alamat1,
        customer_name: nama,
        credit_card: nomorRekening,
        telephone: nomorTelp,
        total_quantity: location.state.data[0].data.quantity,
        total_price: location.state.data[0].data.price,
      },
      params: {
        id_product: location.state.data[0].data.id_product,
        quantity: location.state.data[0].data.quantity,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          MySwal.fire({
            icon: 'success',
            title: 'Berhasil..',
            text: 'Transaksi berhasil dilakukan.',
          });

          page('/user');
        }
      })
      .catch((err) => {
        console.log('err post', err.response.data);
      });
  };

  const handleaddInvoiceCart = (totalPrice, totalQty, listProduct, listQty) => {
    APIRequest({
      method: 'post',
      url: 'api/invoice/addinvoicecart',
      data: {
        id: 0,
        id_user: localStorage.getItem('@idUser'),
        nama: namaDepan + namaBelakang,
        alamat: alamat1,
        customer_name: nama,
        credit_card: nomorRekening,
        telephone: nomorTelp,
        total_quantity: totalQty,
        total_price: totalPrice,
        list_product: listProduct,
        list_quantity: listQty,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          MySwal.fire({
            icon: 'success',
            title: 'Berhasil..',
            text: 'Transaksi berhasil dilakukan.',
          });
          page('/user');
        }
      })
      .catch((err) => {
        console.log('err post', err.response.data);
      });
  };

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <AddressForm
            namaDepan={handleChangeNamaDepan}
            namaBelakang={handleChangeNamaBelakang}
            alamat1={handleChangeAlamat1}
            alamat2={handleChangeAlamat2}
            kota={handleChangeKota}
            provinsi={handleChangeProvinsi}
            kodePost={handleChangeKodePos}
            negara={handleChangeNegara}
          />
        );
      case 1:
        return (
          <PaymentForm
            nama={handleChangeNama}
            nomorRekening={handleChangeNomorRekening}
            nomorTelp={handleChangeNomorTelp}
          />
        );
      case 2:
        return (
          <Review
            nama={nama}
            nomorRekening={nomorRekening}
            nomorTelp={nomorTelp}
            alamat1={alamat1}
            alamat2={alamat2}
            kota={kota}
            provinsi={provinsi}
            kodePos={kodePos}
            negara={negara}
            data={location.state}
          />
        );
      default:
        throw new Error('Unknown step');
    }
  }

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container component="main" maxWidth="md" sx={{ mb: 4, mt: 15 }}>
        <Paper
          variant="outlined"
          sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 }, borderRadius: 8 }}
        >
          <Typography
            component="h1"
            variant="h4"
            align="center"
            sx={{ fontFamily: 'Work Sans' }}
          >
            Checkout
          </Typography>
          <ThemeProvider theme={theme}>
            <Stepper
              activeStep={activeStep}
              sx={{ pt: 3, pb: 5, fontFamily: 'Work Sans' }}
            >
              {steps.map((label) => (
                <Step key={label}>
                  <StepLabel iconColor="red" sx={{ fontFamily: 'Work Sans' }}>
                    {label}
                  </StepLabel>
                </Step>
              ))}
            </Stepper>
          </ThemeProvider>
          <React.Fragment>
            {activeStep === steps.length ? (
              <React.Fragment>
                <Typography
                  variant="h5"
                  gutterBottom
                  sx={{ fontFamily: 'Work Sans' }}
                >
                  Terima kasih atas pembelian anda.
                </Typography>
                <Typography
                  variant="subtitle1"
                  sx={{ fontFamily: 'Work Sans' }}
                >
                  Kami akan mengirimkan email konfirmasi pembelian anda dan
                  memberikan update ketika pesanan anda sudah dikirimkan.
                </Typography>
              </React.Fragment>
            ) : (
              <React.Fragment>
                {getStepContent(activeStep)}
                <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                  {activeStep !== 0 && (
                    <BootstrapButton
                      onClick={handleBack}
                      sx={{ color: 'white', mt: 3, ml: 1 }}
                    >
                      Kembali
                    </BootstrapButton>
                  )}
                  <BootstrapButton
                    loading={loading}
                    variant="contained"
                    sx={{ mt: 3, ml: 3, backgroundColor: '#43ae6b' }}
                    onClick={handleNext}
                  >
                    {activeStep === steps.length - 1
                      ? 'Lakukan pemesanan'
                      : 'Lanjut'}
                  </BootstrapButton>
                </Box>
              </React.Fragment>
            )}
          </React.Fragment>
        </Paper>
      </Container>
    </ThemeProvider>
  );
}
