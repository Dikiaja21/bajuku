import * as React from 'react';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Grid from '@mui/material/Grid';
import CurrencyFormat from 'react-currency-format';

const products = [
  {
    name: 'Product 1',
    desc: 'Sepatu Tsubasa',
    price: 'Rp 9.991',
  },
  {
    name: 'Product 2',
    desc: 'Helm Eyeshield',
    price: 'Rp 3.451',
  },
  {
    name: 'Product 3',
    desc: 'Jubah keadilan',
    price: 'Rp 6.511',
  },
  {
    name: 'Product 4',
    desc: 'Topi jerami',
    price: 'Rp 14.111',
  },
  { name: 'Ongkos kirim', desc: '', price: 'Gratis' },
];

export default function Review(props) {
  const [totalPrice, setTotalPrice] = React.useState(0);

  const calcTotalPrice = () => {
    let result = 0;
    props.data.data.forEach((item) => {
      result += item.data.price * item.data.quantity;
      // console.log(item.data.price);
    });
    setTotalPrice(result);
  };

  // React.useEffect(() => {
  //   console.log(totalPrice);
  // }, [totalPrice])

  React.useEffect(() => {
    calcTotalPrice();
  }, []);
  return (
    <React.Fragment>
      {/* Daftar Produk Dibeli */}
      <Typography variant="h6" gutterBottom sx={{ fontFamily: 'Work Sans' }}>
        Konfirmasi pesanan
      </Typography>
      <List disablePadding>
        {props.data.data.map((product) => (
          <ListItem key={product.data.id_product} sx={{ py: 1, px: 0 }}>
            <ListItemText
              primary={product.data.title}
              secondary={product.data.category_name ?? localStorage.getItem('@name_category')}
            />
            <CurrencyFormat
              value={product.data.price}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'Rp. '}
              renderText={(value) => (
                <Typography variant="body2" sx={{ fontFamily: 'Work Sans' }}>
                  {value}
                </Typography>
              )}
            />
          </ListItem>
        ))}
        {/* Total Harga */}
        <ListItem sx={{ py: 1, px: 0 }}>
          <ListItemText primary="Total" />
          <CurrencyFormat
            value={totalPrice}
            displayType={'text'}
            thousandSeparator={true}
            prefix={'Rp. '}
            renderText={(value) => (
              <Typography
                variant="subtitle1"
                sx={{ fontFamily: 'Work Sans', fontWeight: 700 }}
              >
                {value}
              </Typography>
            )}
          />
        </ListItem>
      </List>
      <Grid container spacing={1}>
        {/* Alamat Pengiriman */}
        <Grid item xs={12} sm={6}>
          <Typography
            variant="h6"
            gutterBottom
            sx={{ mt: 2, fontFamily: 'Work Sans' }}
          >
            Alamat pengiriman
          </Typography>
          <Typography gutterBottom sx={{ fontFamily: 'Work Sans' }}>
            {props.nama}
          </Typography>
          <Typography gutterBottom sx={{ fontFamily: 'Work Sans' }}>
            {props.alamat1}
            {', '}
            {props.alamat2}
            {', '}
            {props.kota}
            {', '}
            {props.provinsi}
            {', '}
            {props.kodePos} {props.negara}
          </Typography>
        </Grid>

        {/* Detail Pembayaran */}
        <Grid item container direction="column" xs={12} sm={6}>
          <Typography
            variant="h6"
            gutterBottom
            sx={{ mt: 2, fontFamily: 'Work Sans' }}
          >
            Detail pembayaran
          </Typography>
          <Grid container>
            <React.Fragment>
              <Grid item xs={6}>
                <Typography gutterBottom sx={{ fontFamily: 'Work Sans' }}>
                  Nama: {props.nama}
                </Typography>
                <Typography gutterBottom sx={{ fontFamily: 'Work Sans' }}>
                  Nomor telpon: {props.nomorTelp}
                </Typography>
                <Typography gutterBottom sx={{ fontFamily: 'Work Sans' }}>
                  Nomor Rekening: {props.nomorRekening}
                </Typography>
              </Grid>
            </React.Fragment>
          </Grid>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
