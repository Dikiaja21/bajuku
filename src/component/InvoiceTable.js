import React, { useState, useEffect } from 'react';
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Paper,
  TableBody,
  Typography,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import { Container } from '@mui/system';
import { useNavigate, useLocation } from 'react-router-dom';
import { APIRequest } from '../axios';
import CurrencyFormat from 'react-currency-format';

const ActionButton = styled(Button)({
  boxShadow: 'none',
  color: 'white',
  textTransform: 'none',
  fontSize: { xs: '14px', md: '18px' },
  padding: '6px 12px',
  border: '1px solid',
  borderRadius: '8px',
  lineHeight: '21px',
  backgroundColor: '#39AB6D',
  borderColor: '#39AB6D',
  width: '180px',
  fontFamily: 'Work Sans',
  fontStyle: 'normal',
  fontWeight: 700,
  '&:hover': {
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6Dc',
    boxShadow: 'none',
  },
  '&:active': {
    boxShadow: 'none',
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6D',
  },
});

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(even)': {
    backgroundColor: 'rgba(57, 171, 109, 0.2);',
  },
}));

export const InvoiceTable = () => {
  const navigate = useNavigate();
  const [listData, setListData] = useState([]);

  const detailInvoice = (id, total_price, tanggal) => {
    let path = `/user/detail`;

    const data = {
      id: id,
      total_price: total_price,
      tanggal: tanggal,
    };

    navigate(path, { state: data });
  };

  const getData = () => {
    // ApiRequest hasil import dari .../axios/index.js
    APIRequest({
      method: 'get',
      url: '/api/Invoice/GetInvoice',
    })
      .then((res) => {
        if (res.status === 200) {
          const newData = res.data.map((item) => {
            // Data sementara
            let tempp = {};

            tempp.id = item.id;
            tempp.id_user = item.id_user;
            tempp.nama = item.nama;
            tempp.alamat = item.alamat;
            tempp.customer_name = item.customer_name;
            tempp.credit_card = item.credit_card;
            tempp.telephone = item.telephone;
            tempp.total_quantity = item.total_quantity;
            tempp.total_price = item.total_price;
            tempp.tanggal_beli = item.tanggal_beli;

            return tempp;
          });

          // Simpan data yang sifatnya sementara ke state asli
          setListData(newData);
        }
      })
      .catch((err) => {
        console.log('err data', err.response.data);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <Container sx={{ mt: 17, mb: 10 }}>
      <Typography
        variant="h5"
        sx={{
          marginTop: { xs: '24px', md: '40px' },
          marginBottom: { xs: '24px', md: '60px' },
          color: '#828282',
          fontFamily: 'Work Sans',
          fontSize: { xs: '18px', md: '24px' },
          fontWeight: 600,
        }}
      >
        Beranda {' > '} <span style={{ color: '#39AB6D' }}>Invoice</span>
      </Typography>
      <Typography
        variant="h5"
        sx={{
          marginBottom: { xs: '40px', md: '51px' },
          color: '#4F4F4F',
          fontFamily: 'Work Sans',
          fontSize: { xs: '18px', md: '24px' },
          fontWeight: 700,
        }}
      >
        Menu Invoice
      </Typography>
      <TableContainer
        container={Paper}
        sx={{
          maxHeight: { xs: '292px', md: '387px' },
          fontFamily: 'Work Sans',
          color: '#4F4F4F',
        }}
      >
        <Table aria-label="invoice table" stickyHeader>
          <TableHead>
            <TableRow
              sx={{
                '& th': {
                  backgroundColor: '#E0E0E0',
                  color: '#4F4F4F',
                  fontSize: { xs: '14px', md: '20px' },
                  fontFamily: 'Work Sans',
                  fontStyle: 'normal',
                  fontWeight: 700,
                  lineHeight: { xs: '16px', md: '23px' },
                  padding: { xs: '14px', md: '20px' },
                },
              }}
            >
              <TableCell align="center">No</TableCell>
              <TableCell align="center">No. Invoice</TableCell>
              <TableCell align="center">Tanggal Beli</TableCell>
              <TableCell align="center">Jumlah Barang</TableCell>
              <TableCell align="center">Total Harga</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {listData.map((item, index) => (
              <StyledTableRow key={item.id}>
                <TableCell
                  align="center"
                  sx={{
                    color: '#4F4F4F',
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontSize: { xs: '14px', md: '20px' },
                    fontWeight: 500,
                  }}
                >
                  {index + 1}
                </TableCell>
                <TableCell
                  align="center"
                  sx={{
                    color: '#4F4F4F',
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontSize: { xs: '14px', md: '20px' },
                    fontWeight: 500,
                  }}
                >
                  {item.id}
                </TableCell>
                <TableCell
                  align="center"
                  sx={{
                    color: '#4F4F4F',
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontSize: { xs: '14px', md: '20px' },
                    fontWeight: 500,
                  }}
                >
                  {item.tanggal_beli}
                </TableCell>
                <TableCell
                  align="center"
                  sx={{
                    color: '#4F4F4F',
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontSize: { xs: '14px', md: '20px' },
                    fontWeight: 500,
                  }}
                >
                  {item.total_quantity}
                </TableCell>
                <CurrencyFormat
                  value={item.total_price}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp. '}
                  renderText={(value) => (
                  <TableCell
                    align="center"
                    sx={{
                      color: '#4F4F4F',
                      fontFamily: 'Work Sans',
                      fontStyle: 'normal',
                      fontSize: { xs: '14px', md: '20px' },
                      fontWeight: 500,
                      }}
                    >
                      {value}
                    </TableCell>
                  )}
                />

                <TableCell align="center">
                  <ActionButton
                    onClick={() => {
                      detailInvoice(
                        item.id,
                        item.total_price,
                        item.tanggal_beli
                      );
                    }}
                  >
                    Rincian
                  </ActionButton>
                </TableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
};
