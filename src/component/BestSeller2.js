import { Button } from '@mui/material';
import './css/bestseller.css';
import CurrencyFormat from 'react-currency-format';

const BestSeller2 = (
  props = {
    data: null,
    onBuy: () => {},
  }
) => {
  return (
    <div>
      <Button
        id="buttonCard"
        onClick={() => {
          // Kirim balik data ke parrent
          props.onBuy({
            url: '/user/buy',
            data: props.data,
          });
        }}
      >
        <div className="CardBestSeller">
          <img
            className="imageCardBestSeller"
            src={props.data.imageContent}
            alt=""
          />
          <div className="titleCard">
            <p style={{ fontWeight: '500' }}>{props.data.title}</p>

            <CurrencyFormat
              value={props.data.price}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'Rp. '}
              renderText={(value) => (
                <p style={{ fontWeight: '600' }}>{value}</p>
              )}
            />
          </div>
        </div>
      </Button>
    </div>
  );
};

export default BestSeller2;
