import { Box } from '@mui/system';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import {
  AiOutlineInstagram,
  AiOutlineMail,
  AiOutlineYoutube,
} from 'react-icons/ai';
import { TbBrandTelegram } from 'react-icons/tb';
import { IconButton, Typography } from '@mui/material';

const Footer = () => {
  return (
    <Box
      sx={{
        bgcolor: '#39AB6D',
        color: 'white',
      }}
    >
      <Container maxWidth="lg" sx={{ py: 6 }}>
        <Grid container spacing={5}>
          <Grid item xs={12} md={4}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                flex: 'none',
                marginBottom: { xs: 0, md: '16px' },
              }}
            >
              <Box sx={{ marginBottom: '16px' }}>
                <Typography
                  sx={{
                    AlignSelf: { xs: 'stretch' },
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontWeight: 400,
                    fontSize: '23px',
                  }}
                >
                  Tentang BAJUKU
                </Typography>
              </Box>
              <Box sx={{ marginBottom: { xs: '0px', md: '16px' } }}>
                <Typography
                  sx={{
                    AlignSelf: { xs: 'stretch' },
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontWeight: 400,
                    fontSize: '16px',
                    lineHeight: '19px',
                  }}
                >
                  Bajuku memiliki tujuan untuk membantu kaum manusia untuk
                  memilih fashion yang mereka sukai dengan gaya dan style ala
                  ala kekinian sehingga kaum manusia dapat bergaya dan tampil
                  maksimal
                </Typography>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={12} sm={4}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                flex: 'none',
                marginBottom: { xs: 0, md: '16px' },
              }}
            >
              <Box sx={{ marginBottom: '16px' }}>
                <Typography
                  sx={{
                    AlignSelf: { xs: 'stretch' },
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontWeight: 400,
                    fontSize: '23px',
                  }}
                >
                  Produk
                </Typography>
              </Box>
              <Box
                sx={{
                  display: { xs: 'grid', md: 'flex' },
                  flexDirection: 'column',
                  gridTemplateColumns: { xs: 'repeat(12, 1fr)' },
                  gap: { xs: 2 },
                }}
              >
                <Box
                  sx={{
                    gridColumn: { xs: 'span 6' },
                  }}
                >
                  <Typography
                    sx={{
                      AlignSelf: { xs: 'stretch' },
                      color: 'white',
                      textDecoration: 'none',
                      fontFamily: 'Work Sans',
                      fontStyle: 'normal',
                      fontWeight: 600,
                      fontSize: { xs: '13px', md: '16px' },
                    }}
                    variant="body1"
                    component="a"
                    href="#head"
                  >
                    › Head
                  </Typography>
                </Box>
                <Box
                  sx={{
                    gridColumn: { xs: 'span 6' },
                  }}
                >
                  <Typography
                    sx={{
                      AlignSelf: { xs: 'stretch' },
                      color: 'white',
                      textDecoration: 'none',
                      fontFamily: 'Work Sans',
                      fontStyle: 'normal',
                      fontWeight: 600,
                      fontSize: { xs: '13px', md: '16px' },
                    }}
                    variant="body1"
                    component="a"
                    href="#body"
                  >
                    › Body
                  </Typography>
                </Box>
                <Box
                  sx={{
                    gridColumn: { xs: 'span 6' },
                  }}
                >
                  <Typography
                    sx={{
                      AlignSelf: { xs: 'stretch' },
                      color: 'white',
                      textDecoration: 'none',
                      fontFamily: 'Work Sans',
                      fontStyle: 'normal',
                      fontWeight: 600,
                      fontSize: { xs: '13px', md: '16px' },
                    }}
                    variant="body1"
                    component="a"
                    href="#pants"
                  >
                    › Pants
                  </Typography>
                </Box>
                <Box
                  sx={{
                    gridColumn: { xs: 'span 6' },
                  }}
                >
                  <Typography
                    sx={{
                      AlignSelf: { xs: 'stretch' },
                      color: 'white',
                      textDecoration: 'none',
                      fontFamily: 'Work Sans',
                      fontStyle: 'normal',
                      fontWeight: 600,
                      fontSize: { xs: '13px', md: '16px' },
                    }}
                    variant="body1"
                    component="a"
                    href="#shoes"
                  >
                    › Shoes
                  </Typography>
                </Box>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={12} sm={4}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                flex: 'none',
              }}
            >
              <Box sx={{ marginBottom: '16px' }}>
                <Typography
                  sx={{
                    AlignSelf: { xs: 'stretch' },
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontWeight: 400,
                    fontSize: { xs: '19px', md: '23px' },
                  }}
                >
                  Kontak
                </Typography>
              </Box>
              <Box sx={{ marginBottom: '16px' }}>
                <Typography
                  sx={{
                    AlignSelf: { xs: 'stretch' },
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontWeight: 400,
                    fontSize: { xs: '12px', md: '16px' },
                  }}
                >
                  Jl. Suka Maju Blok O No 28, Billford, Rock Island, Newland
                </Typography>
              </Box>
              <Box sx={{ marginBottom: '16px' }}>
                <Typography
                  sx={{
                    AlignSelf: { xs: 'stretch' },
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontWeight: 400,
                    fontSize: { xs: '12px', md: '16px' },
                  }}
                >
                  021-12345678
                </Typography>
              </Box>
              <Box>
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'row',
                  }}
                >
                  <Box>
                    <IconButton
                      sx={{
                        bgcolor: 'white',
                        borderRadius: '50%',
                        height: '48px',
                        width: '48px',
                        color: '#39AB6D',
                        mr: '16px',
                        alignItems: 'center',
                        textAlign: 'center',
                        '&:hover': {
                          backgroundColor: '#fbf5df',
                          transition: '200ms ease-in',
                        },
                      }}
                      component="a"
                      href="#instagram"
                    >
                      <AiOutlineInstagram size={70} />
                    </IconButton>
                  </Box>
                  <Box>
                    <IconButton
                      sx={{
                        bgcolor: 'white',
                        borderRadius: '50%',
                        height: '48px',
                        width: '48px',
                        color: '#39AB6D',
                        mr: '16px',
                        alignItems: 'center',
                        textAlign: 'center',
                        '&:hover': {
                          backgroundColor: '#fbf5df',
                          transition: '200ms ease-in',
                        },
                      }}
                      component="a"
                      href="#youtube"
                    >
                      <AiOutlineYoutube size={70} />
                    </IconButton>
                  </Box>
                  <Box>
                    <IconButton
                      sx={{
                        bgcolor: 'white',
                        borderRadius: '50%',
                        height: '48px',
                        width: '48px',
                        color: '#39AB6D',
                        mr: '16px',
                        alignItems: 'center',
                        textAlign: 'center',
                        '&:hover': {
                          backgroundColor: '#fbf5df',
                          transition: '200ms ease-in',
                        },
                      }}
                      component="a"
                      href="#telegram"
                    >
                      <TbBrandTelegram size={70} />
                    </IconButton>
                  </Box>
                  <Box>
                    <IconButton
                      sx={{
                        bgcolor: 'white',
                        borderRadius: '50%',
                        height: '48px',
                        width: '48px',
                        color: '#39AB6D',
                        mr: '16px',
                        alignItems: 'center',
                        textAlign: 'center',
                        '&:hover': {
                          backgroundColor: '#fbf5df',
                          transition: '200ms ease-in',
                        },
                      }}
                      component="a"
                      href="#email"
                    >
                      <AiOutlineMail size={70} />
                    </IconButton>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default Footer;
