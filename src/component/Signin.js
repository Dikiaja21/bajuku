import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import { LoadingButton } from '@mui/lab';

// API Request
import { APIRequest, setAuthToken } from '../axios';

// SweetAlert2
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

const theme = createTheme();

export default function SignIn() {
  const page = useNavigate();
  const MySwal = withReactContent(Swal);

  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [loading, setLoading] = React.useState(false);

  const Login = () => {
    setLoading(true);
    APIRequest({
      method: 'post',
      url: 'api/users/login',
      data: {
        email: email,
        password: password,
      },
    })
      .then((res) => {
        if (res.status == 200) {
          // Save Auth token to LocalStorage
          const jwtToken = 'Bearer ' + res.data.jwtToken;
          setAuthToken(jwtToken);

          if (res.data.role == 1) {
            page('/admin');
            localStorage.setItem('@idRole', res.data.role);
          } else {
            // Redirect to Home Page
            localStorage.setItem('@idUser', res.data.id);
            localStorage.setItem('@idRole', res.data.role);
            page('/user');
            // console.log(res);
          }
        }
      })
      .catch((err) => {
        MySwal.fire({
            icon: 'error',
            title: 'Oops..',
            text: 'Login gagal, ' + err.response.data,
          });
      })
      .finally(() => setLoading(false));
  };

  return (
    <div style={{ marginTop: '120px', marginBottom: '150px' }}>
      <ThemeProvider theme={theme}>
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: '#39AB6D' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Masuk
            </Typography>
            <Box noValidate sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email"
                name="email"
                autoComplete="email"
                autoFocus
                color="success"
                onChange={(e) => setEmail(e.target.value)}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                color="success"
                onChange={(e) => setPassword(e.target.value)}
              />
              <FormControlLabel
                control={<Checkbox value="remember" color="success" />}
                label="Ingat saya"
              />

              <LoadingButton
                loading={loading}
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2, backgroundColor: '#39AB6D' }}
                onClick={() => Login()}
              >
                Masuk
              </LoadingButton>

            {/*              
              // <Grid container>
              //   <Grid item xs>
              //     <Link href="#" variant="body2">
              //       Lupa password?
              //     </Link>
              //   </Grid>
              // </Grid>*/}
            </Box>
          </Box>
        </Container>
      </ThemeProvider>
    </div>
  );
}
