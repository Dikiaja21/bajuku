import { useState, useEffect } from 'react';
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Paper,
  TableBody,
  Typography,
  Grid,
} from '@mui/material';
import { APIRequest } from '../axios';
import { Container } from '@mui/system';
import { styled } from '@mui/material/styles';
import { emptyDataListItem } from '../types';
import { useLocation } from 'react-router-dom';
import CurrencyFormat from 'react-currency-format';

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(even)': {
    backgroundColor: 'rgba(57, 171, 109, 0.2);',
  },
}));

export const DetailInvoiceTable = () => {
  const [listData, setListData] = useState(emptyDataListItem);
  const location = useLocation();

  const getData = () => {
    // ApiRequest hasil import dari .../axios/index.js
    APIRequest({
      method: 'get',
      url: '/api/Invoice/GetItem',
      params: {
        infoId: location.state.id,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          const newData = res.data.map((item) => {
            // Data sementara
            let tempp = {};

            tempp.product_title = item.product_title;
            tempp.category = item.category;
            tempp.quantity = item.quantity;
            tempp.price = item.price;

            return tempp;
          });

          // Simpan data yang sifatnya sementara ke state asli
          setListData(newData);
        }
      })
      .catch((err) => {
        console.log('err data', err.response.data);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <Container sx={{ mt: 18, mb: 10 }}>
      <Typography
        variant="h5"
        sx={{
          marginTop: { xs: '24px', md: '40px' },
          marginBottom: { xs: '24px', md: '60px' },
          color: '#828282',
          fontFamily: 'Work Sans',
          fontSize: { xs: '18px', md: '24px' },
          fontWeight: 600,
        }}
      >
        Beranda {' > '} Invoice {' > '}
        <span style={{ color: '#39AB6D' }}>Rincian Invoice</span>
      </Typography>
      <Typography
        variant="h5"
        sx={{
          marginBottom: { xs: '24px', md: '40px' },
          color: '#4F4F4F',
          fontFamily: 'Work Sans',
          fontSize: { xs: '18px', md: '24px' },
          fontWeight: 700,
        }}
      >
        Rincian Invoice
      </Typography>
      <Typography
        variant="h5"
        sx={{
          marginBottom: { xs: '8px', md: '16px' },
          color: '#4F4F4F',
          fontFamily: 'Work Sans',
          fontSize: { xs: '18px', md: '24px' },
          fontWeight: 500,
        }}
      >
        No. Invoice : {location.state.id}
      </Typography>
      <Grid container sx={{ marginBottom: { xs: '24px', md: '60px' } }}>
        <Grid item md={6}>
          <Typography
            variant="h5"
            sx={{
              color: '#4F4F4F',
              fontFamily: 'Work Sans',
              fontSize: { xs: '18px', md: '24px' },
              fontWeight: 500,
              marginBottom: { xs: '24px', md: 0 },
            }}
          >
            Tanggal Beli : {location.state.tanggal}
          </Typography>
        </Grid>
        <Grid item md={6}>
          <CurrencyFormat
            value={location.state.total_price}
            displayType={'text'}
            thousandSeparator={true}
            prefix={'Rp. '}
            renderText={(value) => (
              <Typography
                variant="h5"
                sx={{
                  color: '#4F4F4F',
                  fontFamily: 'Work Sans',
                  fontSize: { xs: '18px', md: '24px' },
                  fontWeight: 700,
                  textAlign: 'right',
                }}
              >
                Total Harga: {value}
              </Typography>
            )}
          />
        </Grid>
      </Grid>

      <TableContainer
        container={Paper}
        sx={{ maxHeight: '387px', fontFamily: 'Work Sans', color: '#4F4F4F' }}
      >
        <Table aria-label="detail invoice table" stickyHeader>
          <TableHead>
            <TableRow
              sx={{
                '& th': {
                  backgroundColor: '#E0E0E0',
                  color: '#4F4F4F',
                  fontSize: { xs: '14px', md: '20px' },
                  fontFamily: 'Work Sans',
                  fontStyle: 'normal',
                  fontWeight: 700,
                  lineHeight: { xs: '16px', md: '23px' },
                  padding: { xs: '14px', md: '20px' },
                },
              }}
            >
              <TableCell align="center">No</TableCell>
              <TableCell align="center">Nama Barang</TableCell>
              <TableCell align="center">Kategori</TableCell>
              <TableCell align="center">Jumlah Barang</TableCell>
              <TableCell align="center">Harga</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {listData.map((item, index) => (
              <StyledTableRow key={item.id}>
                <TableCell
                  align="center"
                  sx={{
                    color: '#4F4F4F',
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontSize: { xs: '14px', md: '20px' },
                    fontWeight: 500,
                  }}
                >
                  {index + 1}
                </TableCell>
                <TableCell
                  align="center"
                  sx={{
                    color: '#4F4F4F',
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontSize: { xs: '14px', md: '20px' },
                    fontWeight: 500,
                  }}
                >
                  {item.product_title}
                </TableCell>
                <TableCell
                  align="center"
                  sx={{
                    color: '#4F4F4F',
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontSize: { xs: '14px', md: '20px' },
                    fontWeight: 500,
                  }}
                >
                  {item.category}
                </TableCell>
                <TableCell
                  align="center"
                  sx={{
                    color: '#4F4F4F',
                    fontFamily: 'Work Sans',
                    fontStyle: 'normal',
                    fontSize: { xs: '14px', md: '20px' },
                    fontWeight: 500,
                  }}
                >
                  {item.quantity}
                </TableCell>
                <CurrencyFormat
                  value={item.price}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp. '}
                  renderText={(value) => (
                    <TableCell
                      align="center"
                      sx={{
                        color: '#4F4F4F',
                        fontFamily: 'Work Sans',
                        fontStyle: 'normal',
                        fontSize: { xs: '14px', md: '20px' },
                        fontWeight: 500,
                      }}
                    >
                      {value}
                    </TableCell>                  
                  )}
                />

              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
};
