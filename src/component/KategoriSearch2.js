import { Box, Button, Grid } from '@mui/material';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import { useState, useEffect } from 'react';
import './css/search.css';
import './css/produk-search.css';
import { useLocation, useNavigate } from 'react-router-dom';
import { APIRequest } from '../axios';
import { emptyDataList } from '../types';
import { styled } from '@mui/material/styles';
import CurrencyFormat from 'react-currency-format';
import Dialog from './child/FilterCategory';

// Custom Button
const CustomButton = styled(Button)({
  boxShadow: 'none',
  color: '#39AB6D',
  textTransform: 'none',
  fontSize: 18,
  fontStyle: 'normal',
  fontWeight: 700,
  padding: '10px',
  border: '1px solid',
  borderRadius: '8px',
  lineHeight: '21px',
  backgroundColor: 'white',
  borderColor: '#39AB6D',
  justifyContent: 'center',
  alignItems: 'center',
  fontFamily: ['Work Sans'].join(','),
  '&:hover': {
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6D',
    boxShadow: 'none',
    color: 'white',
  },
  '&:active': {
    boxShadow: 'none',
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6D',
  },
});

const Search_ = (
  props = {
    search: '',
  }
) => {
  const [Filter, setFilter] = useState(false);
  const [search, setSearch] = useState(false);
  const [category, setCategory] = useState(emptyDataList);
  const [sorting, setSorting] = useState('new');
  const [onRender, setOnRender] = useState(false);
  const [maxPrice, setMaxPrice] = useState(0);
  const [minPrice, setMinPrice] = useState(0);
  let action = ['action', 'action-active'];
  const location = useLocation();
  const buy = useNavigate();

  // State untuk handle Checklist Category
  const [checkedList, setCheckedList] = useState({
    checkAll: false,
    checkA: false,
    checkB: false,
    checkC: false,
    checkD: false,
  });

  // Data dari Hasil Search
  const [listData, setListData] = useState(emptyDataList);

  const getData = () => {
    // ApiRequest hasil import dari .../axios/index.js
    APIRequest({
      method: 'post',
      url: 'api/products/getproductsearchbycategory',
      params: {
        id_category: location.state.id_category,
      },
      data: {
        search: location.state.search,
        maxPrice: maxPrice,
        minPrice: minPrice,
        sort: sorting,
      },
    })
      .then((res) => {
        if (res.status == 200) {
          const newData = res.data.map((item) => {
            let tempData = {};

            tempData.id = item.id;
            tempData.title = item.title;
            tempData.description = item.description;
            tempData.category = item.fk_category_id;
            tempData.price = item.price;
            tempData.keywords = item.keywords;
            tempData.imageName = item.image_name;
            tempData.imageContent = item.image_content;

            return tempData;
          });

          setListData(newData);
          setOnRender(false);
        }
      })
      .catch((err) => {
        console.log('err data', err.response.data);
      });
  };

  // Method untuk Sorting Produk (Penjualan Terbaru,Terlama, dll)
  const handleChange = (event) => {
    setSorting(event.target.value);
  };

  // Method untuk checkBox label "Semua"
  const handleCheckAll = (e) => {
    if (checkedList.checkAll == false) {
      setCheckedList({
        checkAll: true,
        checkA: true,
        checkB: true,
        checkC: true,
        checkD: true,
      });

      // Isi semua dengan ID Category
      setCategory([1, 2, 3, 4]);
    } else {
      setCheckedList({
        checkAll: false,
        checkA: false,
        checkB: false,
        checkC: false,
        checkD: false,
      });
    }
  };

  // Method untuk "Pulihkan" Category
  const clearCheckCategory = () => {
    setCheckedList({
      checkAll: false,
      checkA: false,
      checkB: false,
      checkC: false,
      checkD: false,
    });
    setCategory(emptyDataList);
    getData();
  };

  // Refresh Data etika location berubah
  useEffect(() => {
    getData();
    console.log(location.state);
  }, [location]);

  useEffect(() => {
    if (onRender == true) {
      getData();
    }
  }, [onRender]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [listData]);

  // Refresh hasil Search ketika State Sorting berubah
  useEffect(() => {
    getData();
  }, [sorting]);

  return (
    <div className="container-search">
      <Grid container spacing={7} marginTop="40px">
        {/* title pilih title1 jika /search & title2 jika /kategori */}
        <Grid item xs={12}>
          <div className="title-search">
            <h1 style={{ fontWeight: '400', color: '#828282' }}>
              Beranda{' > '}
              <span style={{ color: '#39AB6D' }}>
                Kategori : {location.state.title}
              </span>
            </h1>
          </div>
        </Grid>
      </Grid>

      {/* Button filter saat reponsive */}
      <Grid container spacing={2} className="space-grid">
        <Grid item xs={12} className="hidden-active">
          <div className="button-filter">
            <Button
              onClick={() => {
                setFilter(true);
              }}
            >
              <BookmarkIcon /> Filter
            </Button>

            <form>
              <select
                style={{
                  width: '150px',
                  height: '38px',
                  border: '1px solid #828282',
                  fontSize: '14px',
                  fontWeight: '400',
                  color: 'black',
                  borderRadius: '5px',
                  textIndent: '10px',
                }}
                value={sorting}
                onChange={(e) => {
                  handleChange(e);
                }}
              >
                <option value={'new'}>Produk Terbaru</option>
                <option value={'old'}>Produk Terlama</option>
                <option value={'cheapest'}>Produk Termurah</option>
                <option value={'expensive'}>Produk Termahal</option>
              </select>
            </form>
          </div>
        </Grid>

        {/* Title filter saat besar */}
        <Grid item xs={12} className="hidden-responsive">
          <div className="title-content-search">
            <h1 style={{ fontSize: '24px', fontWeight: '700' }}>Filter</h1>

            <form
              style={{ display: 'flex', gap: '20px', alignItems: 'center' }}
            >
              <h1 style={{ fontSize: '18px', fontWeight: '700' }}>Urutkan :</h1>
              <select
                value={sorting}
                onChange={(e) => {
                  handleChange(e);
                }}
              >
                <option value={'new'}>Produk Terbaru</option>
                <option value={'old'}>Produk Terlama</option>
                <option value={'cheapest'}>Produk Termurah</option>
                <option value={'expensive'}>Produk Termahal</option>
              </select>
            </form>
          </div>
        </Grid>

        {/* content filter saat layar besar */}
        <Grid item xs={2.5} className="hidden-responsive">
          <div className="list-kategori">
            <h1 style={{ marginBottom: '20px', fontSize: '18px' }}>Harga</h1>

            <form
              style={{
                marginBottom: '20px',
              }}
            >
              <label>
                Minimal
                <input
                  value={minPrice}
                  style={{ marginBottom: '10px' }}
                  type="number"
                  placeholder="Rp"
                  onChange={(e) => setMinPrice(parseInt(e.target.value))}
                />
              </label>

              <label>
                Maksimal
                <input
                  value={maxPrice}
                  type="number"
                  placeholder="Rp"
                  onChange={(e) => setMaxPrice(parseInt(e.target.value))}
                />
              </label>
            </form>
            <CustomButton
              onClick={() => {
                getData();
              }}
            >
              Terapkan
            </CustomButton>
            <CustomButton
              onClick={() => {
                setMinPrice(0);
                setMaxPrice(0);
                setOnRender(true);
              }}
            >
              Pulihkan
            </CustomButton>
          </div>
        </Grid>

        {/* Container Content Produk */}
        <Grid item xs={12} md={9.5}>
          <Grid item xs={12}>
            <Grid container spacing={2}>
              {/* jika tidak ada hasil yang keluar */}
              {listData.length == 0 ? (
                <Grid item xs={12} md={12} lg={12} xl={12}>
                  <div className="not-found">
                    <h1>Hasil tidak ditemukan</h1>
                  </div>
                </Grid>
              ) : null}
              {listData.map((item) => {
                // jika ada hasil yang keluar
                return (
                  <Grid
                    item
                    xs={6}
                    md={4}
                    lg={3}
                    xl={2.4}
                    className="card-search"
                    key={item.id}
                  >
                    <div
                      className="card-search-produk"
                      onClick={() => {
                        let data = { data: item };
                        buy('/user/buy/' + item.id, { state: data });
                        // console.log(data);
                      }}
                    >
                      <img
                        style={{ margin: 'auto' }}
                        src={item.imageContent}
                        alt="1.png"
                      />

                      <p style={{ fontWeight: '500' }}>{item.title}</p>
                      <CurrencyFormat
                        value={item.price}
                        displayType={'text'}
                        thousandSeparator={true}
                        prefix={'Rp. '}
                        renderText={(value) => (
                          <p style={{ fontWeight: '600' }}>{value}</p>
                        )}
                      />
                    </div>
                  </Grid>
                );
              })}
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <Dialog
        open={Filter}
        onClose={() => setFilter(false)}
        onData={() => {}}
        minPrice={minPrice}
        setMinPrice={setMinPrice}
        maxPrice={maxPrice}
        setMaxPrice={setMaxPrice}
        setOnRender={setOnRender}
        getData={getData}
      />
    </div>
  );
};

export default Search_;
