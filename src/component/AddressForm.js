import React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';

export default function AddressForm(props) {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom sx={{ fontFamily: 'Work Sans' }}>
        Alamat pengiriman
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            color="success"
            id="firstName"
            name="firstName"
            label="Nama depan"
            fullWidth
            autoComplete="given-name"
            variant="standard"
            onChange={props.namaDepan}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            color="success"
            id="lastName"
            name="lastName"
            label="Nama belakang"
            fullWidth
            autoComplete="family-name"
            variant="standard"
            onChange={props.namaBelakang}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            color="success"
            id="address1"
            name="address1"
            label="Alamat"
            fullWidth
            autoComplete="shipping address-line1"
            variant="standard"
            onChange={props.alamat1}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            color="success"
            id="address2"
            name="address2"
            label="Alamat Alternatif"
            fullWidth
            autoComplete="shipping address-line2"
            variant="standard"
            onChange={props.alamat2}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            color="success"
            id="city"
            name="city"
            label="Kota"
            fullWidth
            autoComplete="shipping address-level2"
            variant="standard"
            onChange={props.kota}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            color="success"
            id="state"
            name="state"
            label="Provinsi"
            fullWidth
            variant="standard"
            onChange={props.provinsi}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            color="success"
            id="zip"
            name="zip"
            label="Kode pos"
            fullWidth
            autoComplete="shipping postal-code"
            variant="standard"
            onChange={props.kodePos}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            color="success"
            id="country"
            name="country"
            label="Negara"
            fullWidth
            autoComplete="shipping country"
            variant="standard"
            onChange={props.negara}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
