import { IconButton, Typography } from '@mui/material';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import './css/busket2.css';
import { useNavigate } from 'react-router-dom';

// SweetAlert2
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

const Busket2 = () => {
  const navigate = useNavigate();
  const MySwal = withReactContent(Swal);

  const handleCart = (item = 'false') => {
    if (item == 'true') {
      navigate('/user/busket');
    } else {
      MySwal.fire({
        icon: 'info',
        title: 'Info',
        text: 'Tidak dapat akses keranjang, silahkan untuk melakukan proses login atau register akun terlebih dahulu.'
      });
      navigate('/login');
    }
  };

  return (
    <div>
      <IconButton
        variant="Link"
        id="card2"
        onClick={() => handleCart(localStorage.getItem('@login'))}
      >
        <Typography className="title" style={{ marginRight: '20px' }}>
          Cek Belanjaanmu
        </Typography>
        <ShoppingCartIcon id="icon-shope" />
      </IconButton>
    </div>
  );
};

export default Busket2;
