export const CategorySliderData = [
  {
    image:
      'https://images.unsplash.com/photo-1654714480262-28f8a5c208d8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDEwfGJvOGpRS1RhRTBZfHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
    url: '/invoice',
    name: 'Head',
  },
  {
    image:
      'https://images.unsplash.com/photo-1655219435115-d9f0faccd7cd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDN8Ym84alFLVGFFMFl8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60',
    url: '/invoice',
    name: 'Body',
  },
  {
    image:
      'https://images.unsplash.com/photo-1655212874347-7b8bc1227da7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDR8Ym84alFLVGFFMFl8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60',
    url: '/invoice',
    name: 'Pants',
  },
  {
    image:
      'https://images.unsplash.com/photo-1649133816831-de3d91d25288?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDd8Ym84alFLVGFFMFl8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60',
    url: '/invoice',
    name: 'Foot',
  },
];
