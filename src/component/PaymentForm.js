import * as React from 'react';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';

export default function PaymentForm(props) {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom sx={{ fontFamily: 'Work Sans' }}>
        Detail pembayaran
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <TextField
            required
            color="success"
            id="cardName"
            label="Nama"
            fullWidth
            autoComplete="cc-name"
            variant="standard"
            onChange={props.nama}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            color="success"
            id="paymentMethod"
            label="Nomor Rekening"
            fullWidth
            variant="standard"
            onChange={props.nomorRekening}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            color="success"
            id="noTelp"
            label="Nomor Telpon"
            fullWidth
            variant="standard"
            onChange={props.nomorTelp}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
