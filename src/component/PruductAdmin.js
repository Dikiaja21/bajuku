import { useEffect, useState } from 'react';
import { styled } from '@mui/material/styles';
import { emptyDataList, emptyActionType } from '../types';
import { APIRequest } from '../axios';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import SearchIcon from '@mui/icons-material/Search';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import MyDialog from './child/Dialog';
import { Box } from '@mui/system';

// sini
const CssTextField = styled(TextField)({
  '& label.Mui-focused': {
    color: '#39AB6D',
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: '#39AB6D',
  },
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderColor: '#ced4da',
      borderRadius: 10,
    },
    '&:hover fieldset': {
      borderColor: '#39AB6D',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#39AB6D',
    },
  },
});

const BootstrapButton = styled(Button)({
  boxShadow: 'none',
  textTransform: 'none',
  fontSize: 18,
  fontWeight: 600,
  padding: '6px 12px',
  border: '1px solid',
  borderRadius: 10,
  lineHeight: 1.5,
  backgroundColor: '#39AB6D',
  borderColor: '#39AB6D',
  fontFamily: ['Work Sans'].join(','),
  '&:hover': {
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6D',
    boxShadow: 'none',
  },
  '&:active': {
    boxShadow: 'none',
    backgroundColor: '#39AB6D',
    borderColor: '#39AB6D',
  },
});

export const Coba3 = (
  props = {
    open: false,
    onClose: () => {},
  }
) => {
  // sini
  const [listData, setListData] = useState(emptyDataList);
  //useState untuk add produk
  const [open, setOpen] = useState(false);
  const [type, setType] = useState(emptyActionType);
  const [id, setId] = useState(null);
  const [coba, setCoba] = useState(10);
  const [search, setSearch] = useState("");
  // Get data Product dari API
  const getData = (lmt) => {
    // ApiRequest hasil import dari .../axios/index.js
    APIRequest({
      method: 'get',
      url: 'api/Products/GetProduct',
      params: {
        limit: lmt,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          const newData = res.data.map((item) => {
            // Data sementara
            let tempData = {};

            tempData.id = item.id;
            tempData.title = item.title;
            tempData.desc = item.description;
            tempData.category = item.fk_category_id;
            tempData.price = item.price;
            tempData.keywords = item.keywords;
            tempData.stok = item.stok;
            tempData.imageContent = item.image_content;

            return tempData;
          });

          // Simpan data yang sifatnya sementara ke state asli
          setListData(newData);
        }
      })
      .catch((err) => {
        console.log('err data', err.response.data);
      });
  };

  const postData = (typee = 'add', dataa = {}) => {
    APIRequest({
      method: 'post',
      url: 'api/Products/Add',
      data: {
        id: dataa.id || 0,
        title: dataa.title,
        price: dataa.price,
        description: dataa.desc,
        keywords: dataa.keywords,
        fk_category_id: dataa.category,
        image_content: dataa.imageContent,
        stok: dataa.stok,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          alert('Data Berhasil Ditambah');
          getData(coba);
        }
      })
      .catch((err) => {
        console.log('err post', err.response.data);
      });
  };

  const postEdite = (typee = 'edit', dataa = {}, id) => {
    APIRequest({
      method: 'post',
      url: 'api/Products/Edite',
      data: {
        title: dataa.title,
        price: dataa.price,
        description: dataa.desc,
        keywords: dataa.keywords,
        fk_category_id: dataa.category,
        image_content: dataa.imageContent,
        stok: dataa.stok,
      },
      params: {
        id: id,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          alert('Data Berhasil Diubah');
          getData(coba);
        }
      })
      .catch((err) => {
        console.log('err post', err.response.data);
      });
  };

  const getDelete = (typee = 'delete', id) => {
    APIRequest({
      method: 'delete',
      url: 'api/Products/Delete',
      params: {
        id: id,
      },
    })
      .then((res) => {
        if (res.status === 200) {
          alert('Data Berhasil di Delete');
          getData(coba);
        }
      })
      .catch((err) => {
        console.log('err post', err.response.data);
      });
  };

  useEffect(() => {
    getData(coba);
    handleLoadMore(5);
  }, []);

  const handleLoadMore = (num) => {
    setCoba(coba + num);
  };

  return (
    props.open && (
      <Box
        component="main"
        sx={{
          backgroundColor: (theme) =>
            theme.palette.mode === 'light'
              ? theme.palette.grey[100]
              : theme.palette.grey[900],
          flexGrow: 1,
          height: '100vh',
          overflow: 'auto',
          minWidth: '800px',
        }}
        style={{ marginTop: '65px' }}
      >
        <Container maxWidth="x" sx={{ mt: 5, mb: 4 }}>
          <Grid container spacing={3}>
            {/* Greetings */}
            <Typography
              variant="h4"
              sx={{ fontFamily: 'Work Sans', ml: 3, mt: 3 }}
            >
              Halaman Produk
            </Typography>

            {/* Add Product Button & Product List */}
            <Grid item xs={12}>
              <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                <div style={{ padding: '10px' }}>
                  <div style={{ display: 'flex' }}>
                    {/* Searchbar */}
                    <CssTextField
                      id="input-with-icon-textfield"
                      label="Cari disini"
                      onChange={(e) => setSearch(e.target.value)}
                      InputProps={{
                        endAdornment: (
                          <div>
                            <SearchIcon />
                          </div>
                        ),
                      }}
                      variant="outlined"
                      style={{
                        display: 'flex',
                        flexGrow: 1,
                        marginRight: '10px',
                      }}
                    />

                    {/* Add */}
                    <BootstrapButton
                      variant="contained"
                      onClick={(e) => {
                        e.preventDefault();
                        setOpen(true);
                        setType('add');
                        setId(null);
                      }}
                      style={{
                        width: '120px',
                      }}
                    >
                      + Add
                    </BootstrapButton>
                  </div>
                </div>

                <Grid item xs={12}>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <Grid container spacing={2}>
                        {/* item list */}
                        {
                          listData.filter((item) => {
                            if (search == "") {
                              return item
                            } else if(item.title.toLowerCase().includes(search.toLowerCase())){
                              return item
                            }
                          }).map((item,index) => {
                            return(
                            <Grid
                              item
                              key={item.id}
                              xs={12}
                              sm={6}
                              md={4}
                              lg={3}
                            >
                              <Button style={{ width: '100%' }}>
                                <div
                                  style={{
                                    border: '1px solid black',
                                    borderRadius: '5px',
                                  }}
                                >
                                  <img
                                    className="imageCardBestSeller"
                                    src={item.imageContent}
                                    alt=""
                                  />
                                  <div className="titleAndPrice">
                                    <p style={{ fontWeight: '500' }}>
                                      {item.title}
                                    </p>
                                    <p style={{ fontWeight: '600' }}>
                                      {item.price}
                                    </p>
                                  </div>

                                  <div
                                    style={{
                                      display: 'flex',
                                      justifyContent: 'space-around',
                                      paddingBottom: '10px',
                                    }}
                                  >
                                    <div
                                      onClick={(e) => {
                                        e.preventDefault();
                                        setOpen(true);
                                        setType('edit');
                                        setId(item.id);
                                      }}
                                    >
                                      Edit
                                    </div>

                                    <div
                                      onClick={(e) => {
                                        e.preventDefault();
                                        setOpen(true);
                                        setType('delete');
                                        setId(item.id);
                                      }}
                                    >
                                      Delete
                                    </div>
                                  </div>
                                </div>
                              </Button>
                            </Grid>
                          )
                          })
                        }
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Button
                  style={{
                    color: '#39AB6D',
                    fontFamily: 'Work Sans',
                    fontWeight: 700,
                    marginTop: 30,
                  }}
                  onClick={() => {
                    handleLoadMore(5);
                    getData(coba);
                  }}
                >
                  LOAD MORE
                </Button>
                {/* {loading && <span>LOADING</span>} */}

                {/* Edit dialog */}
                <MyDialog
                  initialData={id ? listData.find((val) => val.id === id) : {}}
                  open={open}
                  onClose={() => setOpen(false)}
                  onAdd={(item) => {
                    postData('add', item);
                  }}
                  onEdit={(item) => {
                    postEdite('edit', item, item.id);
                  }}
                  onDelete={(item) => {
                    getDelete('delete', item.id);
                  }}
                  type={type}
                />
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </Box>
    )
  );
};
