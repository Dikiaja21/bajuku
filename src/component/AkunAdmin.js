import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { Orders } from './ListAkun';

export const Coba1 = (
  props = {
    open: false,
    onclose: () => {},
  }
) => {
  return (
    props.open && (
      <Box
        component="main"
        sx={{
          backgroundColor: (theme) =>
            theme.palette.mode === 'light'
              ? theme.palette.grey[100]
              : theme.palette.grey[900],
          flexGrow: 1,
          height: '100vh',
          overflow: 'auto',
        }}
      >
        <Toolbar />
        <Container maxWidth="x" sx={{ mt: 5, mb: 4 }}>
          <Grid container spacing={3}>
            {/* Greetings */}
            <Typography
              variant="h4"
              sx={{ fontFamily: 'Work Sans', ml: 3, mt: 4 }}
            >
              Selamat Datang, Admin
            </Typography>

            {/* Recent Orders */}
            <Grid item xs={12} style={{}}>
              <Paper
                sx={{
                  p: 2,
                  display: 'flex',
                  flexDirection: 'column',
                  minWidth: '800px',
                }}
                style={{ overflowX: 'scroll' }}
              >
                <Orders />
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </Box>
    )
  );
};
