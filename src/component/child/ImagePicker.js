import React, { useState, useEffect } from 'react';

import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

import { ImagePickerPropTypes, emptyImageData } from '../../types';
import { toBase64 } from './FileConverter';

const ImagePicker = (props = ImagePickerPropTypes) => {
  // handle data
  const [imageData, setImageData] = useState(emptyImageData);
  const imageToDisplay = imageData.imageDataUrl;
  const required = props.required && !imageToDisplay; // required style only applied when image is empty

  const cleanImageObjUrl = () => {
    if (imageData.imageObjUrl) {
      URL.revokeObjectURL(imageData.imageObjUrl);
    }
  };

  const setMergeData = (item = emptyImageData) => {
    setImageData((prevState) => ({
      ...prevState,
      ...item,
    }));
  };

  const resetData = () => {
    setImageData({});
  };

  // lifecycle
  useEffect(() => {
    resetData();

    if (props.initialImageUrl) {
      setMergeData({
        imageObjUrl: props.initialImageUrl,
        imageDataUrl: props.initialImageUrl,
      });
    }
  }, [props.initialImageUrl]);

  // render: using label to trigger hidden input
  return (
    <label
      style={{
        display: 'block',
        height: '220px',
        width: '220px',
        borderRadius: '5px',
        border: required ? '1px solid red' : '1px solid grey',
        position: 'relative',
      }}
      htmlFor="image-picker" // trigger input with id="image-picker"
    >
      {/* hidden input to pick image */}
      <input
        type="file"
        accept="image/*"
        id="image-picker"
        hidden
        disabled={props.disabled}
        onChange={async (e) => {
          const tempImage = e.target.files[0];
          let tempData = emptyImageData;

          cleanImageObjUrl();
          tempData.imageRaw = tempImage;
          tempData.imageObjUrl = URL.createObjectURL(tempImage);
          tempData.imageDataUrl = await toBase64(tempImage);

          setMergeData(tempData);
          props.onChange(tempData);
        }}
      />

      {/* show image */}
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          flexDirection: 'column',
          alignItems: 'center',
          height: '100%',
          width: '100%',
        }}
      >
        {imageToDisplay ? (
          <img
            src={imageToDisplay}
            alt="Add Image"
            style={{ height: '100%', width: '100%', objectFit: 'contain' }}
          />
        ) : (
          <>
            <AddPhotoAlternateIcon
              style={{ height: '80px', width: '80px' }}
              color={props.disabled ? 'disabled' : required ? 'error' : 'black'}
            />
            {required && (
              <span style={{ color: props.disabled ? 'grey' : 'red' }}>
                Required
              </span>
            )}
          </>
        )}
      </div>

      {/* delete button with absolute position (require parent with relative/absolute position) */}
      <div
        style={{
          position: 'absolute',
          top: '10px',
          right: '10px',
          display: imageToDisplay ? 'flex' : 'none',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onClick={(e) => {
          if (!props.disabled) {
            e.preventDefault();
            cleanImageObjUrl();
            resetData();
          }
        }}
      >
        <DeleteForeverIcon
          size="small"
          color={props.disabled ? 'disabled' : required ? 'error' : 'black'}
        />
      </div>

      {props.disabled && (
        <div
          style={{
            height: '220px',
            width: '220px',
            borderRadius: '5px',
            backgroundColor: 'grey',
            opacity: 0.15,
            position: 'absolute',
            top: 0,
            left: 0,
          }}
        />
      )}
    </label>
  );
};

export default ImagePicker;
