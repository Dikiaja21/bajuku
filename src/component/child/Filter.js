import { useState } from 'react';
import { Grid, Button, Box } from '@mui/material';
import { emptyDataList } from '../../types';

const Dialog = (
  props = {
    open: false,
    onClose: () => {},
    checkedAll: false,
    onHandleCheckAll: () => {},
    checkA: false,
    onHandleCheckBoxA: () => {},
    checkB: false,
    onHandleCheckBoxB: () => {},
    checkC: false,
    onHandleCheckBoxC: () => {},
    checkD: false,
    onHandleCheckBoxD: () => {},
    minPrice: false,
    setMinPrice: () => {},
    maxPrice: false,
    setMaxPrice: () => {},
    getData: () => {},
    setOnRender: () => {},
    onClearCheckCategory: () => {}
  }
) => {
  return (
    props.open && (
      <Box className="action">
        <Grid container spacing={2} className="a">
          <Grid item xs={12}>
            <div
              className="pagination"
              onClick={() => {
                props.onClose();
              }}
            />
          </Grid>

          <Grid item xs={12}>
            <div className="title-active">
              <h1>Filter</h1>
              <Button onClick={() => {
                  props.setMinPrice(0);
                  props.setMaxPrice(0);
                  props.setOnRender(true);
                  props.onClearCheckCategory();
              }}>Pulihkan</Button>
            </div>
          </Grid>

          <Grid item xs={12}>
            <div className="title-active">
              <h1>Kategori</h1>
            </div>
          </Grid>

          <Grid item xs={12} className="active-checkbox">
            <form>
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <label htmlFor="checkbox1">
                    <input
                      value="all"
                      checked={props.checkedAll}
                      type="checkbox"
                      id="checkbox1"
                      onChange={props.onHandleCheckAll}
                    />
                    Semua
                  </label>
                </Grid>

                <Grid item xs={6}>
                  <label htmlFor="checkbox2">
                    <input
                      value="2"
                      checked={props.checkA}
                      type="checkbox"
                      id="checkbox2"
                      className="checkmark"
                      onChange={(e) => {
                        props.onHandleCheckBoxA(e, 'checkA');
                      }}
                    />
                    Head
                  </label>
                </Grid>

                <Grid item xs={6}>
                  <label htmlFor="checkbox3">
                    <input
                      value="1"
                      checked={props.checkB}
                      type="checkbox"
                      id="checkbox3"
                      className="checkmark"
                      onChange={(e) => {
                        props.onHandleCheckBoxB(e, 'checkB');
                      }}
                    />
                    Body
                  </label>
                </Grid>

                <Grid item xs={6}>
                  <label htmlFor="checkbox4">
                    <input
                      value="3"
                      checked={props.checkC}
                      type="checkbox"
                      id="checkbox4"
                      className="checkmark"
                      onChange={(e) => {
                        props.onHandleCheckBoxC(e, 'checkC');
                      }}
                    />
                    Pants
                  </label>
                </Grid>

                <Grid item xs={6}>
                  <label htmlFor="checkbox5">
                    <input
                      value="4"
                      checked={props.checkD}
                      type="checkbox"
                      id="checkbox5"
                      className="checkmark"
                      onChange={(e) => {
                        props.onHandleCheckBoxD(e, 'checkD');
                      }}
                    />
                    Shoes
                  </label>
                </Grid>
              </Grid>
            </form>
          </Grid>

          <Grid item xs={12}>
            <div className="title-active">
              <h1>Harga</h1>
            </div>
          </Grid>

          <Grid item xs={12} className="active-harga">
            <form>
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <label>
                    Minimal
                    <input
                      value={props.minPrice}
                      style={{ marginBottom: '10px' }}
                      type="number"
                      placeholder="Rp"
                      onChange={(e) => props.setMinPrice(parseInt(e.target.value))}
                    />
                  </label>
                </Grid>

                <Grid item xs={6}>
                  <label>
                    Maksimal
                    <input
                      value={props.maxPrice}
                      type="number"
                      placeholder="Rp"
                      onChange={(e) => props.setMaxPrice(parseInt(e.target.value))}
                    />
                  </label>
                </Grid>
              </Grid>
            </form>
          </Grid>

          <Grid item xs={12}>
            <div className="button-active">
              <Button
                onClick={() => {
                  props.getData();
                  props.onClose();
                }}
              >
                Terapkan
              </Button>
            </div>
          </Grid>
        </Grid>
      </Box>
    )
  );
};

export default Dialog;
