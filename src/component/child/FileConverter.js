export const toBase64 = (file) => {
  return new Promise((resolve, reject) => {
    let reader = new FileReader();

    reader.onload = () => {
      const base64 = reader.result;
      resolve(base64);
    };
    reader.onerror = (err) => {
      reject(err);
    };

    reader.readAsDataURL(file);
  });
};
