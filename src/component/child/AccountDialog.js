import { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import TextField from "@mui/material/TextField";

import { emptyActionType, emptyDataObj } from "../../types";

function AccountDialog(
  props = {
    initialData: emptyDataObj,
    open: false,
    onClose: (item = emptyDataObj) => {},
    onAdd: (item = emptyDataObj) => {},
    onEdit: (item = emptyDataObj) => {},
    type: emptyActionType,
  }
) {
  // variable
  const [first_name, setfirst_name] = useState("");
  const [last_name, setlast_name] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [id_role, setid_role] = useState(1);
  const [active, setActive] = useState(true);

  const disabled = props.type === "delete";
  const fullWidthStyle = {
    width: "90%",
    marginTop: "10px",
    marginBottom: "10px",
  };

  // function
  const validationOk = () => {
    let errMsg = "";

    if (!first_name) {
      errMsg += "Nama depan tidak boleh kosong";
    }
    if (!last_name) {
      errMsg += "Nama belakang tidak boleh kosong";
    }
    if (!email) {
      errMsg += "Email tidak boleh kosong";
    }
    if (!password) {
      errMsg += "Password tidak boleh kosong";
    }
    if (id_role < 1 || id_role > 2) {
      errMsg += "Id role tidak sesuai";
    }
    if (errMsg) {
      alert(errMsg);
      return false;
    } else {
      return true;
    }
  };

  // lifecycle
  useEffect(() => {
    if (props.open) {
      // init
      setfirst_name(props.initialData.first_name ?? "");
      setlast_name(props.initialData.last_name ?? "");
      setEmail(props.initialData.email ?? "");
      setPassword(props.initialData.password ?? "");
      setid_role(props.initialData.id_role ?? 1);
      setActive(props.initialData.active ?? true);
    }
  }, [props.open]);

  // render
  return (
    <Dialog open={props.open} onClose={props.onClose}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          width: { xs: "300px", sm: "600px" },
        }}
      >
        {/* JUDUL POP UP DIALOG */}
        <Typography variant='h5' textAlign='center' sx={{ mt: 3, mb: 2 }}>
          {props.type === "add"
            ? "Tambah Akun"
            : props.type === "edit"
            ? "Edit Akun"
            : "Hapus Akun"}
        </Typography>
        {/* INPUT NAMA DEPAN */}
        <TextField
          label={"Nama Depan"}
          color='success'
          value={first_name}
          onChange={(e) => setfirst_name(e.target.value)}
          error={first_name ? false : true}
          helperText={first_name ? "" : "Tidak boleh kosong"}
          disabled={disabled}
          sx={fullWidthStyle}
        />
        {/* INPUT NAMA BELAKANG */}
        <TextField
          label={"Nama Belakang"}
          value={last_name}
          color='success'
          onChange={(e) => setlast_name(e.target.value)}
          error={last_name ? false : true}
          helperText={last_name ? "" : "Tidak boleh kosong"}
          disabled={disabled}
          sx={fullWidthStyle}
        />
        {/* INPUT EMAIL */}
        <TextField
          label={"Email"}
          color='success'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          error={email ? false : true}
          helperText={email ? "" : "Tidak boleh kosong"}
          disabled={disabled}
          sx={fullWidthStyle}
        />
        {/* INPUT PASSWORD */}
        <TextField
          label={"Password"}
          color='success'
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          error={password ? false : true}
          helperText={password ? "" : "Tidak boleh kosong"}
          disabled={disabled}
          sx={fullWidthStyle}
          type='password'
        />
        {/* INPUT ID ROLE */}
        <TextField
          label={"Tipe Akun"}
          value={id_role}
          color='success'
          onChange={(e) => setid_role(Number(e.target.value))}
          error={id_role ? false : true}
          helperText={id_role ? "" : "Tidak boleh kosong"}
          disabled={disabled}
          sx={fullWidthStyle}
        />
        <br />
        {props.type === "delete" && (
          <Typography variant='body1' textAlign='center'>
            Apakah anda yakin ingin menghapus akun?
          </Typography>
        )}
        {/* Save button */}
        <Button
          variant='contained'
          sx={fullWidthStyle}
          color={disabled ? "error" : "success"}
          onClick={() => {
            if (validationOk() === false) {
              return; // stop function
            }

            const newData = {
              first_name: first_name,
              last_name: last_name,
              email: email,
              password: password,
              id_role: id_role,
              active: active,
            };

            // handle type
            if (props.type === "add") {
              props.onAdd({
                ...props.initialData,
                ...newData,
              });
            } else if (props.type === "edit") {
              props.onEdit({
                ...props.initialData,
                ...newData,
              });
            } else if (props.type === "delete") {
              props.onDelete(props.initialData);
            }

            // close modal
            props.onClose();
          }}
        >
          {props.type === "add"
            ? "Add"
            : props.type === "edit"
            ? "Edit"
            : "Delete"}
        </Button>
      </Box>
    </Dialog>
  );
}

export default AccountDialog;
