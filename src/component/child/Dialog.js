import { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import { emptyActionType, emptyDataObj } from '../../types';
import { categoryList } from './categoryList';
import ImagePicker from './ImagePicker';

function MyDialog(
  props = {
    initialData: emptyDataObj,
    open: false,
    onClose: (item = emptyDataObj) => {},
    onAdd: (item = emptyDataObj) => {},
    onEdit: (item = emptyDataObj) => {},
    type: emptyActionType,
  }
) {
  // variable
  const [title, setTitle] = useState('');
  const [desc, setDesc] = useState('');
  const [price, setPrice] = useState(0);
  const [category, setCategory] = useState(1);
  const [keywords, setKeywords] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const [stok, setStok] = useState('');

  const disabled = props.type === 'delete';
  const fullWidthStyle = {
    width: '90%',
    marginTop: '10px',
    marginBottom: '10px',
  };

  // function
  const validationOk = () => {
    let errMsg = '';

    if (!title) {
      errMsg += 'Title cannot empty; ';
    }
    if (price < 1) {
      errMsg += 'Price cannot lower than 1; ';
    }
    if (stok < 1) {
      errMsg += 'Stok cannot lower than 1; ';
    }
    if (errMsg) {
      alert(errMsg);
      return false;
    } else {
      return true;
    }
  };

  // lifecycle
  useEffect(() => {
    if (props.open) {
      // init
      setTitle(props.initialData.title ?? '');
      setImageUrl(props.initialData.imageContent ?? '');
      setDesc(props.initialData.desc ?? '');
      setPrice(props.initialData.price ?? 0);
      setStok(props.initialData.stok ?? 0);
      setCategory(props.initialData.category ?? 1);
      setKeywords(props.initialData.keywords ?? '');
    }
  }, [props.open]);

  // render
  return (
    <Dialog open={props.open} onClose={props.onClose}>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          width: { xs: '300px', sm: '600px' },
        }}
      >
        {/* Title */}
        <Typography variant="h5" textAlign="center">
          {props.type === 'add'
            ? 'Add Item'
            : props.type === 'edit'
            ? 'Edit item'
            : 'Delete item'}
        </Typography>
        {/* Image */}
        <ImagePicker
          disabled={disabled}
          onChange={(item) => setImageUrl(item.imageDataUrl)}
          // required
          initialImageUrl={imageUrl}
        />
        {/* INPUT TITLE */}
        <TextField
          label={'Title'}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          error={title ? false : true}
          helperText={title ? '' : 'Cannot empty'}
          disabled={disabled}
          sx={fullWidthStyle}
        />
        {/* INPUT DESC */}
        <TextField
          label={'Description'}
          value={desc}
          onChange={(e) => setDesc(e.target.value)}
          disabled={disabled}
          sx={fullWidthStyle}
          multiline
          rows={4}
        />
        {/* INPUT PRICE */}
        <TextField
          label={'Price'}
          value={String(price)}
          onChange={(e) => setPrice(Number(e.target.value))}
          error={price > 0 ? false : true}
          helperText={price > 0 ? '' : 'Cannot zero'}
          disabled={disabled}
          sx={fullWidthStyle}
          type="number"
        />
        {/* INPUT PRICE */}
        <TextField
          label={'Stok'}
          value={String(stok)}
          onChange={(e) => setStok(Number(e.target.value))}
          error={price > 0 ? false : true}
          helperText={price > 0 ? '' : 'Cannot zero'}
          disabled={disabled}
          sx={fullWidthStyle}
          type="number"
        />
        
        <Box sx={fullWidthStyle}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Category</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={String(category)}
              label="Category"
              onChange={(e) => setCategory(Number(e.target.value))}
            >
              {categoryList.map((item, index) => {
                return (
                  <MenuItem key={index} value={String(item.id)}>
                    {item.value}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </Box>
        {/* INPUT KEYWORDS */}
        <TextField
          label={'Search Keywords'}
          value={keywords}
          onChange={(e) => setKeywords(e.target.value)}
          disabled={disabled}
          sx={fullWidthStyle}
        />
        <br />
        {props.type === 'delete' && (
          <Typography variant="body1" textAlign="center">
            Are you sure to delete?
          </Typography>
        )}
        {/* Save button */}
        <Button
          variant="contained"
          sx={fullWidthStyle}
          color={disabled ? 'error' : 'primary'}
          onClick={() => {
            if (validationOk() === false) {
              return; // stop function
            }

            const newData = {
              title: title,
              desc: desc,
              price: price,
              category: category,
              keywords: keywords,
              imageContent: imageUrl,
              stok: stok,
              imageName: '', // belum dikasih logic ini
            };

            // handle type
            if (props.type === 'add') {
              props.onAdd({
                ...props.initialData,
                ...newData,
              });
            } else if (props.type === 'edit') {
              props.onEdit({
                ...props.initialData,
                ...newData,
              });
            } else if (props.type === 'delete') {
              props.onDelete(props.initialData);
            }

            // close modal
            props.onClose();
          }}
        >
          {props.type === 'add'
            ? 'Add'
            : props.type === 'edit'
            ? 'Edit'
            : 'Delete'}
        </Button>
      </Box>
    </Dialog>
  );
}
export default MyDialog;
