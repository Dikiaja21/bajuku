import { useState } from 'react';
import { Grid, Button, Box } from '@mui/material';
import { emptyDataList } from '../../types';

const Dialog = (
  props = {
    open: false,
    onClose: () => {},
    minPrice: false,
    setMinPrice: () => {},
    maxPrice: false,
    setMaxPrice: () => {},
    getData: () => {},
    setOnRender: () => {},
    onClearCheckCategory: () => {}
  }
) => {
  
  return (
    props.open && (
      <Box className="action">
        <Grid container spacing={2} className="a">
          <Grid item xs={12}>
            <div
              className="pagination"
              onClick={() => {
                props.onClose();
              }}
            />
          </Grid>

          <Grid item xs={12}>
            <div className="title-active">
              <h1>Filter</h1>
              <Button onClick={() => {
                  props.setMinPrice(0);
                  props.setMaxPrice(0);
                  props.setOnRender(true);
                  props.onClose();
              }}>Pulihkan</Button>
            </div>
          </Grid>

          <Grid item xs={12}>
            <div className="title-active">
              <h1>Harga</h1>
            </div>
          </Grid>

          <Grid item xs={12} className="active-harga">
            <form>
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <label>
                    Minimal
                    <input
                      value={props.minPrice}
                      style={{ marginBottom: '10px' }}
                      type="number"
                      placeholder="Rp"
                      onChange={(e) => props.setMinPrice(parseInt(e.target.value))}
                    />
                  </label>
                </Grid>

                <Grid item xs={6}>
                  <label>
                    Maksimal
                    <input
                      value={props.maxPrice}
                      type="number"
                      placeholder="Rp"
                      onChange={(e) => props.setMaxPrice(parseInt(e.target.value))}
                    />
                  </label>
                </Grid>
              </Grid>
            </form>
          </Grid>

          <Grid item xs={12}>
            <div className="button-active">
              <Button
                onClick={() => {
                  props.getData();
                  props.onClose();
                }}
              >
                Terapkan
              </Button>
            </div>
          </Grid>
        </Grid>
      </Box>
    )
  );
};

export default Dialog;
