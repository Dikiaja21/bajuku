import Axios from 'axios';

export const APIRequest = Axios.create({
  baseURL: localStorage.getItem('baseUrl'),
  "Content-Type": "application/json"
});

export const setAuthToken = (authToken = "") => {
  // Set Axios Auth Header
  APIRequest.defaults.headers.common["Authorization"] = authToken;

  // Save to local storage
  localStorage.setItem('@token', authToken);
}

export const getAuthToken = () => {
  return localStorage.getItem('@token');
}