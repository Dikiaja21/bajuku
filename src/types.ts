export interface IDataObj {
  id?: number,
  title?: string,
  desc?: string,
  price?: number,
  category?: string | number,    
  keywords?: string,
  imageContent?: string,
  imageName?: string
};

export interface Invoice {
  id: number,
  id_user: number,
  nama: string,
  alamat: string,
  customer_name: string,
  credit_card: string,
  telephone: string,
  total_quantity: number,
  total_price: number
};

export interface InvoiceList {
  product_title: string,
  category : string,
  quantity : number,
  price : number
};

export const emptyDataObj: IDataObj = {};
export const emptyDataList: IDataObj[] = [];
export const emptyDataListInvoice: Invoice[] = [];
export const emptyDataListItem: InvoiceList[] = [];
export type TActionType = 'add' | 'edit' | 'delete';
export const emptyActionType: TActionType = "add";


// IMAGE PICKER
export interface IImageData {
  imageRaw?: Blob,
  imageObjUrl?: string,
  imageDataUrl?: string
}

export interface IImagePicker {
  /** `Optional` */
  onChange?: (item: IImageData) => void,
  /** `Optional` */
  disabled?: boolean,
  /** `Optional` */
  required?: boolean,
  /** `Optional` */
  initialImageUrl?: string,
}

export const emptyImageData: IImageData = {};
export const ImagePickerPropTypes: IImagePicker = {};