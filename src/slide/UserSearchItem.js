import Search2 from '../component/Search2';
import { useLocation } from 'react-router-dom';

export const UserSearchItem = () => {
  const location = useLocation();

  return (
    <div style={{ marginTop: '100px' }}>
      <Search2 search={location.state.search} />
    </div>
  );
};
