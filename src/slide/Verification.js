import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import Skeleton from '@mui/material/Skeleton';

// Css
import '../component/css/verification.css';

// API Request
import { APIRequest } from '../axios';

const Verification = () => {
  // Get Token dari paramater url
  const { token } = useParams();

  const [msg, setMsg] = useState('');
  const [isError, setIsError] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    APIRequest({
      method: 'post',
      url: 'api/users/verifyemail/' + token,
    })
      .then((res) => {
        setLoading(true);
        if (res.status == 200) {
          setMsg('Success');
        }
        setLoading(false);
      })
      .catch((err) => {
        console.log('err verif', err.response.data);
        setMsg('Failed');
        setIsError(true);
        setLoading(false);
      });
  }, []);

  return (
    <div
      style={{
        height: '70vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 50,
        marginTop: 100,
      }}
    >
      <div style={{ textAlign: 'center', lineHeight: '30px' }}>
        <center>
          {loading ? (
            <Skeleton width={300} height={300}></Skeleton>
          ) : (
            <img
              class="image-verification"
              src={require('../component/images/Success.png')}
            ></img>
          )}
          {loading ? (
            <Skeleton width="30%" height={40}></Skeleton>
          ) : (
            <h1>{msg}</h1>
          )}
          {loading ? (
            <Skeleton width="70%" height={26}></Skeleton>
          ) : isError ? (
            <h3>Something when wrong</h3>
          ) : (
            <h3>Email Verification</h3>
          )}
          {loading ? (
            <Skeleton width="90%" height={18}></Skeleton>
          ) : isError ? (
            <p>
              Verifikasi email gagal dilakukan, lakukan verifikasi ulang kembali
            </p>
          ) : (
            <p>
              Yeay, email mu sudah berhasil diverifikasi.. silahkan lakukan
              login pada halaman <Link to="/login">Bajuku Login</Link>
            </p>
          )}
        </center>
      </div>
    </div>
  );
};

export default Verification;
