import Search from '../component/Search';
import { useLocation } from 'react-router-dom';

const SearchItem = () => {
  const location = useLocation();

  return (
    <div style={{ marginTop: '100px' }}>
      <Search search={location.state.search} />
    </div>
  );
};

export default SearchItem;
