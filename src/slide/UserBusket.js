import Keranjang from '../component/Keranjang';
import BestSeller2 from '../component/BestSeller2';
import { emptyDataList } from '../types';
import { useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { APIRequest } from '../axios';

export const UserBusket = () => {
  // Tampung data dari Axios
  const [listData, setListData] = useState(emptyDataList);
  const buy = useNavigate();

  // Fungsi looping untuk component BestSeller2 di Page Buy
  const renderList = (item, index) => {
    return (
      <BestSeller2
        key={index}
        data={item}
        onBuy={(data) => {
          // Redirect ke halaman By sesuai id product
          // Mengirim data state ke page Buy
          buy(data.url + '/' + data.data.id, { state: data });
          getData();
        }}
      ></BestSeller2>
    );
  };

  // Get data product dari API
  const getData = () => {
    // ApiRequest hasil import dari .../axios/index.js
    APIRequest({
      method: 'get',
      url: 'api/products/getproductlisting',
    })
      .then((res) => {
        if (res.status === 200) {
          const newData = res.data.map((item) => {
            // Data sementara
            let tempData = {};

            tempData.id = item.id;
            tempData.title = item.title;
            tempData.category_name = item.name;
            tempData.description = item.description;
            tempData.price = item.price;
            tempData.imageContent = item.image_content;

            return tempData;
          });

          // Simpan data sementara ke state asli
          setListData(newData);
        }
      })
      .catch((err) => {
        console.log('err data', err.response.data);
      });
  };

  useEffect(() => {
    if (listData.length == 0) {
      getData();
    }
  }, []);

  // Get data dari state yang dikirim dari fungsi renderList()
  const location = useLocation();

  return (
    <div style={{ marginTop: '100px' }}>
      <Keranjang />
      <div className="container">
        <h1 className="titleContent">Rekomendasi Lainnya</h1>
        <div className="containerCard">
          {
            // Looping komponen BestSeller2 sesuai data dari listData
            listData.map((item, index) => renderList(item, index))
          }
        </div>
      </div>
    </div>
  );
};
