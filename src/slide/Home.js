import Client from '../component/Client';
import Busket from '../component/Busket';
import BestSeller from '../component/BestSeller';
// import Category from '../component/Category';
import Kategori from '../component/Kategori';

import { useState, useEffect } from 'react';
import { emptyDataList } from '../types';
import { APIRequest } from '../axios';
import '../component/css/bestseller.css';
import { useLocation, useNavigate } from 'react-router-dom';

const Home = () => {
  // Tampung data dari axios
  const [listData, setListData] = useState(emptyDataList);

  const buy = useNavigate();
  const location = window.location.pathname;

  // Get data Product dari API
  const getData = () => {
    // ApiRequest hasil import dari .../axios/index.js
    APIRequest({
      method: 'get',
      url: 'api/products/getproductlisting',
    })
      .then((res) => {
        if (res.status === 200) {
          const newData = res.data.map((item) => {
            // Data sementara
            let tempData = {};

            tempData.id = item.id;
            tempData.title = item.title;
            tempData.category_name = item.name;
            tempData.description = item.description;
            tempData.price = item.price;
            tempData.imageContent = item.image_content;

            return tempData;
          });

          // Simpan data yang sifatnya sementara ke state asli
          setListData(newData);
        }
      })
      .catch((err) => {
        console.log('err data', err.response.data);
      });
  };

  // Fungsi looping component BestSeller
  const renderList = (item, index) => {
    return (
      <BestSeller
        location
        key={index}
        data={item}
        onBuy={(data) => {
          // Redirect ke halaman By sesuai id product
          // Mengirim data state ke page Buy
          buy(data.url + '/' + data.data.id, { state: data });
        }}
      ></BestSeller>
    );
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <Busket />
      <Client />
      <div className="container">
        <h1 className="titleContent">Penjualan terbaik</h1>
        <div className="containerCard">
          {
            // Looping komponen BestSeller sesuai data dari listData
            listData.map((item, index) => renderList(item, index))
          }
        </div>
      </div>

      <Kategori />
    </div>
  );
};

export default Home;
