import Footer from '../component/Footer';
import NavbarLogin from '../component/NavbarLogin';
import { Outlet } from 'react-router-dom';

export const UserRoote = () => {
  return (
    <div>
      <NavbarLogin />
      <Outlet />
      <Footer />
    </div>
  );
};
