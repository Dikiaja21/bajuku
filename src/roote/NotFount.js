export const NotFount = () => {
  return (
    <div
      style={{
        height: '70vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <div style={{ textAlign: 'center', lineHeight: '30px' }}>
        <h1>404</h1>
        <h3>Page Not Found</h3>
        <p>Pastikan anda sudah Login terlebih dahulu</p>
      </div>
    </div>
  );
};
