import Footer from '../component/Footer';
import Navbar from '../component/Navbar';
import { Outlet } from 'react-router-dom';

export const NoUserRoote = () => {
  return (
    <div>
      <Navbar />
      <Outlet />
      <Footer />
    </div>
  );
};
